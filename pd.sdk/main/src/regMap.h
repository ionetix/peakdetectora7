/*
 * regMap.h
 *
 *  Created on: Mar 20, 2015
 *      Author: nusher
 */

#ifndef REGMAP_H_
#define REGMAP_H_

// Write to any register sets (or resets) the FREEZE bit
#define FREEZE				0x44A00000
#define F1DBM				0x44A00000
#define F1DBM_HI			0xC4A00000
#define R1DBM				0x44A00004
#define R1DBM_HI			0xC4A00004
#define F2DBM				0x44A00008
#define F2DBM_HI			0xC4A00008
#define R2DBM				0x44A0000C
#define R2DBM_HI			0xC4A0000C
#define F3DBM				0x44A00010
#define F3DBM_HI			0xC4A00010
#define R3DBM				0x44A00014
#define R3DBM_HI			0xC4A00014
#define F4DBM				0x44A00018
#define F4DBM_HI			0xC4A00018
#define R4DBM				0x44A0001C
#define R4DBM_HI			0xC4A0001C
#define F5DBM				0x44A00020
#define F5DBM_HI			0xC4A00020
#define R5DBM				0x44A00024
#define R5DBM_HI			0xC4A00024
#define F6DBM				0x44A00028
#define F6DBM_HI			0xC4A00028
#define R6DBM				0x44A0002C
#define R6DBM_HI			0xC4A0002C
#define F1VP				0x44A00030
#define F1VP_HI				0xC4A00030
#define R1VP				0x44A00034
#define R1VP_HI				0xC4A00034
#define F2VP				0x44A00038
#define F2VP_HI				0xC4A00038
#define R2VP				0x44A0003C
#define R2VP_HI				0xC4A0003C
#define F3VP				0x44A00040
#define F3VP_HI				0xC4A00040
#define R3VP				0x44A00044
#define R3VP_HI				0xC4A00044
#define F4VP				0x44A00048
#define F4VP_HI				0xC4A00048
#define R4VP				0x44A0004C
#define R4VP_HI				0xC4A0004C
#define F5VP				0x44A00050
#define F5VP_HI				0xC4A00050
#define R5VP				0x44A00054
#define R5VP_HI				0xC4A00054
#define F6VP				0x44A00058
#define F6VP_HI				0xC4A00058
#define R6VP				0x44A0005C
#define R6VP_HI				0xC4A0005C
#define F1RAW				0x44A00060
#define F1RAW_HI			0xC4A00060
#define R1RAW				0x44A00064
#define R1RAW_HI			0xC4A00064
#define F2RAW				0x44A00068
#define F2RAW_HI			0xC4A00068
#define R2RAW				0x44A0006C
#define R2RAW_HI			0xC4A0006C
#define F3RAW				0x44A00070
#define F3RAW_HI			0xC4A00070
#define R3RAW				0x44A00074
#define R3RAW_HI			0xC4A00074
#define F4RAW				0x44A00078
#define F4RAW_HI			0xC4A00078
#define R4RAW				0x44A0007C
#define R4RAW_HI			0xC4A0007C
#define F5RAW				0x44A00080
#define F5RAW_HI			0xC4A00080
#define R5RAW				0x44A00084
#define R5RAW_HI			0xC4A00084
#define F6RAW				0x44A00088
#define F6RAW_HI			0xC4A00088
#define R6RAW				0x44A0008C
#define R6RAW_HI			0xC4A0008C

// Modbus RTU connection on spare connector on back panel
#define MODBUS_RX			0x40600000
#define MODBUS_TX			0x40600004
#define MODBUS_STS			0x40600008

// Hardware calculation for Modbus RTU CRC and timeouts
#define INIT_CRCIN			0x72600000
#define UPDATE_CRCIN		0x72600004
#define READ_CRCIN			0x72600004
#define INIT_CRCOUT			0x72600008
#define UPDATE_CRCOUT		0x7260000C
#define READ_CRCOUT			0x7260000C
#define CLR_TIMER			0x72600010
#define READ_TIMER			0x72600010
#define MODBUS_ADDR			0x72600014

#endif /* REGMAP_H_ */
