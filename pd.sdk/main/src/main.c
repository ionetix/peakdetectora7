#include "xil_io.h"
#include "regmap.h"

#define MODBUSTIMEOUT		100000		// 1 ms

// Modbus register map
const int numInputRegs = 72;
const u32 inputRegs[72] = {
		F1DBM, F1DBM_HI, R1DBM, R1DBM_HI, F2DBM, F2DBM_HI, R2DBM, R2DBM_HI,
		F3DBM, F3DBM_HI, R3DBM, R3DBM_HI, F4DBM, F4DBM_HI, R4DBM, R4DBM_HI,
		F5DBM, F5DBM_HI, R5DBM, R5DBM_HI, F6DBM, F6DBM_HI, R6DBM, R6DBM_HI,
		F1VP,  F1VP_HI,  R1VP,  R1VP_HI,  F2VP,  F2VP_HI,  R2VP,  R2VP_HI,
		F3VP,  F3VP_HI,  R3VP,  R3VP_HI,  F4VP,  F4VP_HI,  R4VP,  R4VP_HI,
		F5VP,  F5VP_HI,  R5VP,  R5VP_HI,  F6VP,  F6VP_HI,  R6VP,  R6VP_HI,
		F1RAW, F1RAW_HI, R1RAW, R1RAW_HI, F2RAW, F2RAW_HI, R2RAW, R2RAW_HI,
		F3RAW, F3RAW_HI, R3RAW, R3RAW_HI, F4RAW, F4RAW_HI, R4RAW, R4RAW_HI,
		F5RAW, F5RAW_HI, R5RAW, R5RAW_HI, F6RAW, F6RAW_HI, R6RAW, R6RAW_HI};

// scales value and converts to floating point, but returns as a u32 type for easier Modbus transmission
u32 scaleToPLC(u32 value) {
	float temp;
	temp = 3.814697266E-6 * (float) ((signed) value);
	return *((u32 *) &temp);
}
void respond(unsigned char * message);
int modbus_rcvd() {
	//xil_printf("\r\nEntering modbus recvd\r\n");
	return Xil_In32(MODBUS_STS) & 0x00000001;
}
unsigned char modbus_getchar() {
	//xil_printf("\r\nEntering MODBUS GETCHAR\r\n");
	return Xil_In32(MODBUS_RX);
}
void modbus_putchar(unsigned char c) {
	// if TX FIFO is full, wait to send byte
	//xil_printf("\r\nEntering modbus putchar\r\n");
	while (Xil_In32(MODBUS_STS) & 0x00000008);
	Xil_Out32(MODBUS_TX, c);
}
void init_crcin() {
	//xil_printf("\r\nEntering init crc in\r\n");
	Xil_Out32(INIT_CRCIN, 1);
}
void update_crcin(unsigned char val) {
	//xil_printf("\r\nEntering update crc\r\n");
	Xil_Out32(UPDATE_CRCIN, (u32) val);
}
u16 get_crcin() {
	return Xil_In32(READ_CRCIN);
}
void init_crcout() {
	//xil_printf("\r\nEntering init crc out\r\n");
	Xil_Out32(INIT_CRCOUT, 1);
}
void update_crcout(unsigned char val) {
	//xil_printf("\r\nEntering update crc out\r\n");
	Xil_Out32(UPDATE_CRCOUT, (u32) val);
}
u16 get_crcout() {
	return Xil_In32(READ_CRCOUT);
}
void reset_modbustimer() {
	//xil_printf("\r\nEntering reset modbus timer\r\n");
	Xil_Out32(CLR_TIMER, 1);
}
u32 read_modbustimer() {
	return Xil_In32(READ_TIMER);
}
void respondModbus(unsigned char * message);
int main() {
	// modbus state register
	// 0 = waiting for transmission
	// 1 = waiting for next byte
	int modbus_count = 0;
	int modbus_messagesize = 8;
	unsigned char modbus_packet[8];
	u16 crc = 0;

	while (1) {
		// byte received via Modbus link

		if (modbus_rcvd()) {
			reset_modbustimer();
			if (modbus_count < modbus_messagesize) {
				modbus_packet[modbus_count] = modbus_getchar();
				if (modbus_count < modbus_messagesize - 2)
					update_crcin(modbus_packet[modbus_count]);
				modbus_count++;
			}
		}
		// If end of message, process it
		if (modbus_count == modbus_messagesize) {
			// check crc, if valid respond to message
			crc = (((u16) modbus_packet[modbus_messagesize - 1]) << 8)
					| modbus_packet[modbus_messagesize - 2];
			// only commands 1-6 and 15-16 are implemented so far
			// support for Modbus command 6 (write single holding register) has been removed
			if ((modbus_packet[0] == (unsigned char) Xil_In32(MODBUS_ADDR))
					&& (get_crcin() == crc)
					&& (modbus_packet[1] == 4))
				respondModbus(modbus_packet);
			// prepare for next message
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
		}
		// If receiving message and delay is too long between bytes, abort message
		else if ((modbus_count != modbus_messagesize) && modbus_count
				&& (read_modbustimer() > MODBUSTIMEOUT)) {
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
		}
	}
}
void respondModbus(unsigned char * message) {
	unsigned char i;
	unsigned char tempChar;
	u16 tempU16;
	u32 tempU32;
	int index;
	int count;
	u32 pointer;
	Xil_Out32(FREEZE, 1);// freeze all read-only regs before processing any command
	// slave address and function
	init_crcout();
	update_crcout((unsigned char) Xil_In32(MODBUS_ADDR));
	modbus_putchar((unsigned char) Xil_In32(MODBUS_ADDR));
	update_crcout(message[1]);
	modbus_putchar(message[1]);
	index = (((int) message[2]) << 8) + message[3];
	count = (((int) message[4]) << 8) + message[5];
	switch (message[1]) {
	// read input registers
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted right by 16 bits
	case 4:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempU16 <<= 1;
		tempChar = (tempU16 > 254) ? 254 : tempU16;
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bytes
		for (i = 0; i < count; i++) {
			if (index < numInputRegs) {
				pointer = inputRegs[index];
				if ((i == 0) || (pointer != (inputRegs[index - 1] & 0x7FFFFFFF)))
					tempU32 = scaleToPLC(Xil_In32(pointer & 0x7FFFFFFF));
			} else
				tempU32 = 0xFFFFFFFF;
			if (pointer & 0x80000000)
				tempU16 = tempU32 >> 16;
			else
				tempU16 = tempU32;
			index++;
			tempChar = tempU16 >> 8;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			tempChar = tempU16;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;
	}
	// add crc to end of message
	tempU16 = get_crcout();
	modbus_putchar(tempU16);
	modbus_putchar(tempU16 >> 8);
	Xil_Out32(FREEZE, 0);// unfreeze all read-only regs after processing any command
}
