// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
// Date        : Thu Jan 16 08:20:34 2020
// Host        : Windows10 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               Z:/Documents/git/master/pd/pd.srcs/sources_1/bd/pd/ip/pd_modbusCRC_axi_0_0/pd_modbusCRC_axi_0_0_sim_netlist.v
// Design      : pd_modbusCRC_axi_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a15tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "pd_modbusCRC_axi_0_0,modbusCRC_axi_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "modbusCRC_axi_v1_0,Vivado 2017.3" *) 
(* NotValidForBitStream *)
module pd_modbusCRC_axi_0_0
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 64, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0 inst
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "modbusCRC_axi_v1_0" *) 
module pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0
   (S_AXI_ARREADY,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_arvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_arvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI modbusCRC_axi_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "modbusCRC_axi_v1_0_S00_AXI" *) 
module pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI
   (S_AXI_ARREADY,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_arvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_ARREADY;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_arvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready_i_1_n_0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire busyIn;
  wire busyIn_i_1_n_0;
  wire busyIn_i_3_n_0;
  wire busyOut;
  wire busyOut_i_1_n_0;
  wire [15:0]crcIn;
  wire crcIn19_out;
  wire \crcIn[0]_i_1_n_0 ;
  wire \crcIn[10]_i_1_n_0 ;
  wire \crcIn[11]_i_1_n_0 ;
  wire \crcIn[12]_i_1_n_0 ;
  wire \crcIn[13]_i_1_n_0 ;
  wire \crcIn[14]_i_1_n_0 ;
  wire \crcIn[15]_i_1_n_0 ;
  wire \crcIn[15]_i_2_n_0 ;
  wire \crcIn[15]_i_3_n_0 ;
  wire \crcIn[15]_i_4_n_0 ;
  wire \crcIn[1]_i_1_n_0 ;
  wire \crcIn[2]_i_1_n_0 ;
  wire \crcIn[3]_i_1_n_0 ;
  wire \crcIn[4]_i_1_n_0 ;
  wire \crcIn[5]_i_1_n_0 ;
  wire \crcIn[6]_i_1_n_0 ;
  wire \crcIn[7]_i_1_n_0 ;
  wire \crcIn[8]_i_1_n_0 ;
  wire \crcIn[9]_i_1_n_0 ;
  wire [15:0]crcOut;
  wire crcOut15_out;
  wire \crcOut[0]_i_1_n_0 ;
  wire \crcOut[10]_i_1_n_0 ;
  wire \crcOut[11]_i_1_n_0 ;
  wire \crcOut[12]_i_1_n_0 ;
  wire \crcOut[13]_i_1_n_0 ;
  wire \crcOut[14]_i_1_n_0 ;
  wire \crcOut[15]_i_1_n_0 ;
  wire \crcOut[15]_i_2_n_0 ;
  wire \crcOut[15]_i_3_n_0 ;
  wire \crcOut[15]_i_4_n_0 ;
  wire \crcOut[1]_i_1_n_0 ;
  wire \crcOut[2]_i_1_n_0 ;
  wire \crcOut[3]_i_1_n_0 ;
  wire \crcOut[4]_i_1_n_0 ;
  wire \crcOut[5]_i_1_n_0 ;
  wire \crcOut[6]_i_1_n_0 ;
  wire \crcOut[7]_i_1_n_0 ;
  wire \crcOut[8]_i_1_n_0 ;
  wire \crcOut[9]_i_1_n_0 ;
  wire [2:0]ctrIn;
  wire \ctrIn[0]_i_1_n_0 ;
  wire \ctrIn[1]_i_1_n_0 ;
  wire \ctrIn[2]_i_1_n_0 ;
  wire [2:0]ctrOut;
  wire \ctrOut[0]_i_1_n_0 ;
  wire \ctrOut[1]_i_1_n_0 ;
  wire \ctrOut[2]_i_1_n_0 ;
  wire local;
  wire [31:0]localTimer;
  wire [31:1]localTimer0;
  wire localTimer0_carry__0_i_1_n_0;
  wire localTimer0_carry__0_i_2_n_0;
  wire localTimer0_carry__0_i_3_n_0;
  wire localTimer0_carry__0_i_4_n_0;
  wire localTimer0_carry__0_n_0;
  wire localTimer0_carry__0_n_1;
  wire localTimer0_carry__0_n_2;
  wire localTimer0_carry__0_n_3;
  wire localTimer0_carry__1_i_1_n_0;
  wire localTimer0_carry__1_i_2_n_0;
  wire localTimer0_carry__1_i_3_n_0;
  wire localTimer0_carry__1_i_4_n_0;
  wire localTimer0_carry__1_n_0;
  wire localTimer0_carry__1_n_1;
  wire localTimer0_carry__1_n_2;
  wire localTimer0_carry__1_n_3;
  wire localTimer0_carry__2_i_1_n_0;
  wire localTimer0_carry__2_i_2_n_0;
  wire localTimer0_carry__2_i_3_n_0;
  wire localTimer0_carry__2_i_4_n_0;
  wire localTimer0_carry__2_n_0;
  wire localTimer0_carry__2_n_1;
  wire localTimer0_carry__2_n_2;
  wire localTimer0_carry__2_n_3;
  wire localTimer0_carry__3_i_1_n_0;
  wire localTimer0_carry__3_i_2_n_0;
  wire localTimer0_carry__3_i_3_n_0;
  wire localTimer0_carry__3_i_4_n_0;
  wire localTimer0_carry__3_n_0;
  wire localTimer0_carry__3_n_1;
  wire localTimer0_carry__3_n_2;
  wire localTimer0_carry__3_n_3;
  wire localTimer0_carry__4_i_1_n_0;
  wire localTimer0_carry__4_i_2_n_0;
  wire localTimer0_carry__4_i_3_n_0;
  wire localTimer0_carry__4_i_4_n_0;
  wire localTimer0_carry__4_n_0;
  wire localTimer0_carry__4_n_1;
  wire localTimer0_carry__4_n_2;
  wire localTimer0_carry__4_n_3;
  wire localTimer0_carry__5_i_1_n_0;
  wire localTimer0_carry__5_i_2_n_0;
  wire localTimer0_carry__5_i_3_n_0;
  wire localTimer0_carry__5_i_4_n_0;
  wire localTimer0_carry__5_n_0;
  wire localTimer0_carry__5_n_1;
  wire localTimer0_carry__5_n_2;
  wire localTimer0_carry__5_n_3;
  wire localTimer0_carry__6_i_1_n_0;
  wire localTimer0_carry__6_i_2_n_0;
  wire localTimer0_carry__6_i_3_n_0;
  wire localTimer0_carry__6_n_2;
  wire localTimer0_carry__6_n_3;
  wire localTimer0_carry_i_1_n_0;
  wire localTimer0_carry_i_2_n_0;
  wire localTimer0_carry_i_3_n_0;
  wire localTimer0_carry_i_4_n_0;
  wire localTimer0_carry_n_0;
  wire localTimer0_carry_n_1;
  wire localTimer0_carry_n_2;
  wire localTimer0_carry_n_3;
  wire \localTimer[0]_i_1_n_0 ;
  wire \localTimer[10]_i_1_n_0 ;
  wire \localTimer[11]_i_1_n_0 ;
  wire \localTimer[12]_i_1_n_0 ;
  wire \localTimer[13]_i_1_n_0 ;
  wire \localTimer[14]_i_1_n_0 ;
  wire \localTimer[15]_i_1_n_0 ;
  wire \localTimer[15]_i_2_n_0 ;
  wire \localTimer[15]_i_3_n_0 ;
  wire \localTimer[16]_i_1_n_0 ;
  wire \localTimer[17]_i_1_n_0 ;
  wire \localTimer[18]_i_1_n_0 ;
  wire \localTimer[19]_i_1_n_0 ;
  wire \localTimer[1]_i_1_n_0 ;
  wire \localTimer[20]_i_1_n_0 ;
  wire \localTimer[21]_i_1_n_0 ;
  wire \localTimer[22]_i_1_n_0 ;
  wire \localTimer[23]_i_1_n_0 ;
  wire \localTimer[23]_i_2_n_0 ;
  wire \localTimer[23]_i_3_n_0 ;
  wire \localTimer[24]_i_1_n_0 ;
  wire \localTimer[25]_i_1_n_0 ;
  wire \localTimer[26]_i_1_n_0 ;
  wire \localTimer[27]_i_1_n_0 ;
  wire \localTimer[28]_i_1_n_0 ;
  wire \localTimer[29]_i_1_n_0 ;
  wire \localTimer[2]_i_1_n_0 ;
  wire \localTimer[30]_i_1_n_0 ;
  wire \localTimer[31]_i_10_n_0 ;
  wire \localTimer[31]_i_11_n_0 ;
  wire \localTimer[31]_i_12_n_0 ;
  wire \localTimer[31]_i_13_n_0 ;
  wire \localTimer[31]_i_14_n_0 ;
  wire \localTimer[31]_i_15_n_0 ;
  wire \localTimer[31]_i_16_n_0 ;
  wire \localTimer[31]_i_17_n_0 ;
  wire \localTimer[31]_i_18_n_0 ;
  wire \localTimer[31]_i_19_n_0 ;
  wire \localTimer[31]_i_1_n_0 ;
  wire \localTimer[31]_i_20_n_0 ;
  wire \localTimer[31]_i_21_n_0 ;
  wire \localTimer[31]_i_22_n_0 ;
  wire \localTimer[31]_i_23_n_0 ;
  wire \localTimer[31]_i_24_n_0 ;
  wire \localTimer[31]_i_25_n_0 ;
  wire \localTimer[31]_i_2_n_0 ;
  wire \localTimer[31]_i_3_n_0 ;
  wire \localTimer[31]_i_4_n_0 ;
  wire \localTimer[31]_i_5_n_0 ;
  wire \localTimer[31]_i_6_n_0 ;
  wire \localTimer[31]_i_7_n_0 ;
  wire \localTimer[31]_i_8_n_0 ;
  wire \localTimer[31]_i_9_n_0 ;
  wire \localTimer[3]_i_1_n_0 ;
  wire \localTimer[4]_i_1_n_0 ;
  wire \localTimer[5]_i_1_n_0 ;
  wire \localTimer[6]_i_1_n_0 ;
  wire \localTimer[7]_i_1_n_0 ;
  wire \localTimer[7]_i_2_n_0 ;
  wire \localTimer[7]_i_3_n_0 ;
  wire \localTimer[8]_i_1_n_0 ;
  wire \localTimer[9]_i_1_n_0 ;
  wire local_i_1_n_0;
  wire local_i_2_n_0;
  wire local_i_3_n_0;
  wire local_i_4_n_0;
  wire local_i_5_n_0;
  wire p_0_in;
  wire [2:0]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire sel;
  wire [2:0]sel0;
  wire [7:0]slaveAddr;
  wire slaveAddr0;
  wire \slaveAddr[7]_i_2_n_0 ;
  wire \slaveAddr[7]_i_3_n_0 ;
  wire \slaveAddr[7]_i_4_n_0 ;
  wire slv_reg_rden__0;
  wire timer0;
  wire \timer[0]_i_10_n_0 ;
  wire \timer[0]_i_11_n_0 ;
  wire \timer[0]_i_12_n_0 ;
  wire \timer[0]_i_4_n_0 ;
  wire \timer[0]_i_5_n_0 ;
  wire \timer[0]_i_6_n_0 ;
  wire \timer[0]_i_7_n_0 ;
  wire \timer[0]_i_8_n_0 ;
  wire \timer[0]_i_9_n_0 ;
  wire [31:0]timer_reg;
  wire \timer_reg[0]_i_3_n_0 ;
  wire \timer_reg[0]_i_3_n_1 ;
  wire \timer_reg[0]_i_3_n_2 ;
  wire \timer_reg[0]_i_3_n_3 ;
  wire \timer_reg[0]_i_3_n_4 ;
  wire \timer_reg[0]_i_3_n_5 ;
  wire \timer_reg[0]_i_3_n_6 ;
  wire \timer_reg[0]_i_3_n_7 ;
  wire \timer_reg[12]_i_1_n_0 ;
  wire \timer_reg[12]_i_1_n_1 ;
  wire \timer_reg[12]_i_1_n_2 ;
  wire \timer_reg[12]_i_1_n_3 ;
  wire \timer_reg[12]_i_1_n_4 ;
  wire \timer_reg[12]_i_1_n_5 ;
  wire \timer_reg[12]_i_1_n_6 ;
  wire \timer_reg[12]_i_1_n_7 ;
  wire \timer_reg[16]_i_1_n_0 ;
  wire \timer_reg[16]_i_1_n_1 ;
  wire \timer_reg[16]_i_1_n_2 ;
  wire \timer_reg[16]_i_1_n_3 ;
  wire \timer_reg[16]_i_1_n_4 ;
  wire \timer_reg[16]_i_1_n_5 ;
  wire \timer_reg[16]_i_1_n_6 ;
  wire \timer_reg[16]_i_1_n_7 ;
  wire \timer_reg[20]_i_1_n_0 ;
  wire \timer_reg[20]_i_1_n_1 ;
  wire \timer_reg[20]_i_1_n_2 ;
  wire \timer_reg[20]_i_1_n_3 ;
  wire \timer_reg[20]_i_1_n_4 ;
  wire \timer_reg[20]_i_1_n_5 ;
  wire \timer_reg[20]_i_1_n_6 ;
  wire \timer_reg[20]_i_1_n_7 ;
  wire \timer_reg[24]_i_1_n_0 ;
  wire \timer_reg[24]_i_1_n_1 ;
  wire \timer_reg[24]_i_1_n_2 ;
  wire \timer_reg[24]_i_1_n_3 ;
  wire \timer_reg[24]_i_1_n_4 ;
  wire \timer_reg[24]_i_1_n_5 ;
  wire \timer_reg[24]_i_1_n_6 ;
  wire \timer_reg[24]_i_1_n_7 ;
  wire \timer_reg[28]_i_1_n_1 ;
  wire \timer_reg[28]_i_1_n_2 ;
  wire \timer_reg[28]_i_1_n_3 ;
  wire \timer_reg[28]_i_1_n_4 ;
  wire \timer_reg[28]_i_1_n_5 ;
  wire \timer_reg[28]_i_1_n_6 ;
  wire \timer_reg[28]_i_1_n_7 ;
  wire \timer_reg[4]_i_1_n_0 ;
  wire \timer_reg[4]_i_1_n_1 ;
  wire \timer_reg[4]_i_1_n_2 ;
  wire \timer_reg[4]_i_1_n_3 ;
  wire \timer_reg[4]_i_1_n_4 ;
  wire \timer_reg[4]_i_1_n_5 ;
  wire \timer_reg[4]_i_1_n_6 ;
  wire \timer_reg[4]_i_1_n_7 ;
  wire \timer_reg[8]_i_1_n_0 ;
  wire \timer_reg[8]_i_1_n_1 ;
  wire \timer_reg[8]_i_1_n_2 ;
  wire \timer_reg[8]_i_1_n_3 ;
  wire \timer_reg[8]_i_1_n_4 ;
  wire \timer_reg[8]_i_1_n_5 ;
  wire \timer_reg[8]_i_1_n_6 ;
  wire \timer_reg[8]_i_1_n_7 ;
  wire [3:2]NLW_localTimer0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_localTimer0_carry__6_O_UNCONNECTED;
  wire [3:3]\NLW_timer_reg[28]_i_1_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_1_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_1_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_1_in[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_1_in[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_1_in[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_1_in[2]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_bvalid),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_awvalid),
        .I5(s00_axi_wvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[0]),
        .I3(sel0[1]),
        .I4(crcIn[0]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(localTimer[0]),
        .I1(local),
        .I2(sel0[1]),
        .I3(slaveAddr[0]),
        .I4(sel0[0]),
        .I5(timer_reg[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[10]),
        .I3(sel0[1]),
        .I4(crcIn[10]),
        .O(reg_data_out[10]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[10]_i_2 
       (.I0(localTimer[10]),
        .I1(sel0[1]),
        .I2(timer_reg[10]),
        .I3(sel0[0]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[11]),
        .I3(sel0[1]),
        .I4(crcIn[11]),
        .O(reg_data_out[11]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[11]_i_2 
       (.I0(localTimer[11]),
        .I1(sel0[1]),
        .I2(timer_reg[11]),
        .I3(sel0[0]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[12]),
        .I3(sel0[1]),
        .I4(crcIn[12]),
        .O(reg_data_out[12]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[12]_i_2 
       (.I0(localTimer[12]),
        .I1(sel0[1]),
        .I2(timer_reg[12]),
        .I3(sel0[0]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[13]),
        .I3(sel0[1]),
        .I4(crcIn[13]),
        .O(reg_data_out[13]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[13]_i_2 
       (.I0(localTimer[13]),
        .I1(sel0[1]),
        .I2(timer_reg[13]),
        .I3(sel0[0]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[14]),
        .I3(sel0[1]),
        .I4(crcIn[14]),
        .O(reg_data_out[14]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[14]_i_2 
       (.I0(localTimer[14]),
        .I1(sel0[1]),
        .I2(timer_reg[14]),
        .I3(sel0[0]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[15]),
        .I3(sel0[1]),
        .I4(crcIn[15]),
        .O(reg_data_out[15]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[15]_i_2 
       (.I0(localTimer[15]),
        .I1(sel0[1]),
        .I2(timer_reg[15]),
        .I3(sel0[0]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[16]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[16]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[16]),
        .O(reg_data_out[16]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[17]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[17]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[17]),
        .O(reg_data_out[17]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[18]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[18]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[18]),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[19]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[19]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[19]),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[1]),
        .I3(sel0[1]),
        .I4(crcIn[1]),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[1]_i_2 
       (.I0(localTimer[1]),
        .I1(sel0[1]),
        .I2(slaveAddr[1]),
        .I3(sel0[0]),
        .I4(timer_reg[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[20]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[20]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[20]),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[21]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[21]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[21]),
        .O(reg_data_out[21]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[22]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[22]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[22]),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[23]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[23]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[23]),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[24]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[24]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[24]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[25]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[25]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[25]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[26]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[26]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[26]),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[27]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[27]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[27]),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[28]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[28]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[28]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[29]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[29]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[29]),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[2]),
        .I3(sel0[1]),
        .I4(crcIn[2]),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[2]_i_2 
       (.I0(localTimer[2]),
        .I1(sel0[1]),
        .I2(slaveAddr[2]),
        .I3(sel0[0]),
        .I4(timer_reg[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[30]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[30]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[30]),
        .O(reg_data_out[30]));
  LUT5 #(
    .INIT(32'hA0080008)) 
    \axi_rdata[31]_i_1 
       (.I0(sel0[2]),
        .I1(timer_reg[31]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(localTimer[31]),
        .O(reg_data_out[31]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[3]),
        .I3(sel0[1]),
        .I4(crcIn[3]),
        .O(reg_data_out[3]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[3]_i_2 
       (.I0(localTimer[3]),
        .I1(sel0[1]),
        .I2(slaveAddr[3]),
        .I3(sel0[0]),
        .I4(timer_reg[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[4]),
        .I3(sel0[1]),
        .I4(crcIn[4]),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[4]_i_2 
       (.I0(localTimer[4]),
        .I1(sel0[1]),
        .I2(slaveAddr[4]),
        .I3(sel0[0]),
        .I4(timer_reg[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[5]),
        .I3(sel0[1]),
        .I4(crcIn[5]),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[5]_i_2 
       (.I0(localTimer[5]),
        .I1(sel0[1]),
        .I2(slaveAddr[5]),
        .I3(sel0[0]),
        .I4(timer_reg[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[6]),
        .I3(sel0[1]),
        .I4(crcIn[6]),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[6]_i_2 
       (.I0(localTimer[6]),
        .I1(sel0[1]),
        .I2(slaveAddr[6]),
        .I3(sel0[0]),
        .I4(timer_reg[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[7]),
        .I3(sel0[1]),
        .I4(crcIn[7]),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[7]_i_2 
       (.I0(localTimer[7]),
        .I1(sel0[1]),
        .I2(slaveAddr[7]),
        .I3(sel0[0]),
        .I4(timer_reg[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[8]),
        .I3(sel0[1]),
        .I4(crcIn[8]),
        .O(reg_data_out[8]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[8]_i_2 
       (.I0(localTimer[8]),
        .I1(sel0[1]),
        .I2(timer_reg[8]),
        .I3(sel0[0]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(crcOut[9]),
        .I3(sel0[1]),
        .I4(crcIn[9]),
        .O(reg_data_out[9]));
  LUT4 #(
    .INIT(16'h8830)) 
    \axi_rdata[9]_i_2 
       (.I0(localTimer[9]),
        .I1(sel0[1]),
        .I2(timer_reg[9]),
        .I3(sel0[0]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h00000000AEEEEEEE)) 
    busyIn_i_1
       (.I0(crcIn19_out),
        .I1(busyIn),
        .I2(ctrIn[0]),
        .I3(ctrIn[1]),
        .I4(ctrIn[2]),
        .I5(\crcIn[15]_i_1_n_0 ),
        .O(busyIn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h02)) 
    busyIn_i_2
       (.I0(busyIn_i_3_n_0),
        .I1(p_1_in[1]),
        .I2(busyIn),
        .O(crcIn19_out));
  LUT5 #(
    .INIT(32'h00800000)) 
    busyIn_i_3
       (.I0(s00_axi_wstrb[1]),
        .I1(s00_axi_wstrb[0]),
        .I2(p_1_in[0]),
        .I3(p_1_in[2]),
        .I4(S_AXI_WREADY),
        .O(busyIn_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    busyIn_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(busyIn_i_1_n_0),
        .Q(busyIn),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AEEEEEEE)) 
    busyOut_i_1
       (.I0(crcOut15_out),
        .I1(busyOut),
        .I2(ctrOut[0]),
        .I3(ctrOut[1]),
        .I4(ctrOut[2]),
        .I5(\crcOut[15]_i_1_n_0 ),
        .O(busyOut_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h08)) 
    busyOut_i_2
       (.I0(busyIn_i_3_n_0),
        .I1(p_1_in[1]),
        .I2(busyOut),
        .O(crcOut15_out));
  FDRE #(
    .INIT(1'b0)) 
    busyOut_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(busyOut_i_1_n_0),
        .Q(busyOut),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h56A6)) 
    \crcIn[0]_i_1 
       (.I0(crcIn[0]),
        .I1(crcIn[1]),
        .I2(crcIn19_out),
        .I3(s00_axi_wdata[0]),
        .O(\crcIn[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[10]_i_1 
       (.I0(crcIn[10]),
        .I1(crcIn19_out),
        .I2(crcIn[11]),
        .O(\crcIn[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[11]_i_1 
       (.I0(crcIn[11]),
        .I1(crcIn19_out),
        .I2(crcIn[12]),
        .O(\crcIn[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[12]_i_1 
       (.I0(crcIn[12]),
        .I1(crcIn19_out),
        .I2(crcIn[13]),
        .O(\crcIn[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h8BB8)) 
    \crcIn[13]_i_1 
       (.I0(crcIn[13]),
        .I1(crcIn19_out),
        .I2(crcIn[0]),
        .I3(crcIn[14]),
        .O(\crcIn[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[14]_i_1 
       (.I0(crcIn[14]),
        .I1(crcIn19_out),
        .I2(crcIn[15]),
        .O(\crcIn[14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \crcIn[15]_i_1 
       (.I0(p_1_in[0]),
        .I1(p_1_in[2]),
        .I2(\crcIn[15]_i_4_n_0 ),
        .O(\crcIn[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \crcIn[15]_i_2 
       (.I0(busyIn),
        .I1(crcIn19_out),
        .O(\crcIn[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[15]_i_3 
       (.I0(crcIn[15]),
        .I1(crcIn19_out),
        .I2(crcIn[0]),
        .O(\crcIn[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFE0000)) 
    \crcIn[15]_i_4 
       (.I0(s00_axi_wstrb[3]),
        .I1(s00_axi_wstrb[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(S_AXI_WREADY),
        .I5(p_1_in[1]),
        .O(\crcIn[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[1]_i_1 
       (.I0(s00_axi_wdata[1]),
        .I1(crcIn[1]),
        .I2(crcIn19_out),
        .I3(crcIn[2]),
        .O(\crcIn[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[2]_i_1 
       (.I0(s00_axi_wdata[2]),
        .I1(crcIn[2]),
        .I2(crcIn19_out),
        .I3(crcIn[3]),
        .O(\crcIn[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[3]_i_1 
       (.I0(s00_axi_wdata[3]),
        .I1(crcIn[3]),
        .I2(crcIn19_out),
        .I3(crcIn[4]),
        .O(\crcIn[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[4]_i_1 
       (.I0(s00_axi_wdata[4]),
        .I1(crcIn[4]),
        .I2(crcIn19_out),
        .I3(crcIn[5]),
        .O(\crcIn[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[5]_i_1 
       (.I0(s00_axi_wdata[5]),
        .I1(crcIn[5]),
        .I2(crcIn19_out),
        .I3(crcIn[6]),
        .O(\crcIn[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[6]_i_1 
       (.I0(s00_axi_wdata[6]),
        .I1(crcIn[6]),
        .I2(crcIn19_out),
        .I3(crcIn[7]),
        .O(\crcIn[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcIn[7]_i_1 
       (.I0(s00_axi_wdata[7]),
        .I1(crcIn[7]),
        .I2(crcIn19_out),
        .I3(crcIn[8]),
        .O(\crcIn[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[8]_i_1 
       (.I0(crcIn[8]),
        .I1(crcIn19_out),
        .I2(crcIn[9]),
        .O(\crcIn[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[9]_i_1 
       (.I0(crcIn[9]),
        .I1(crcIn19_out),
        .I2(crcIn[10]),
        .O(\crcIn[9]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[0]_i_1_n_0 ),
        .Q(crcIn[0]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[10]_i_1_n_0 ),
        .Q(crcIn[10]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[11]_i_1_n_0 ),
        .Q(crcIn[11]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[12]_i_1_n_0 ),
        .Q(crcIn[12]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[13]_i_1_n_0 ),
        .Q(crcIn[13]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[14]_i_1_n_0 ),
        .Q(crcIn[14]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[15]_i_3_n_0 ),
        .Q(crcIn[15]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[1]_i_1_n_0 ),
        .Q(crcIn[1]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[2]_i_1_n_0 ),
        .Q(crcIn[2]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[3]_i_1_n_0 ),
        .Q(crcIn[3]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[4]_i_1_n_0 ),
        .Q(crcIn[4]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[5]_i_1_n_0 ),
        .Q(crcIn[5]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[6]_i_1_n_0 ),
        .Q(crcIn[6]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[7]_i_1_n_0 ),
        .Q(crcIn[7]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[8]_i_1_n_0 ),
        .Q(crcIn[8]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[9]_i_1_n_0 ),
        .Q(crcIn[9]),
        .S(\crcIn[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h56A6)) 
    \crcOut[0]_i_1 
       (.I0(crcOut[0]),
        .I1(crcOut[1]),
        .I2(crcOut15_out),
        .I3(s00_axi_wdata[0]),
        .O(\crcOut[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[10]_i_1 
       (.I0(crcOut[10]),
        .I1(crcOut15_out),
        .I2(crcOut[11]),
        .O(\crcOut[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[11]_i_1 
       (.I0(crcOut[11]),
        .I1(crcOut15_out),
        .I2(crcOut[12]),
        .O(\crcOut[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[12]_i_1 
       (.I0(crcOut[12]),
        .I1(crcOut15_out),
        .I2(crcOut[13]),
        .O(\crcOut[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h8BB8)) 
    \crcOut[13]_i_1 
       (.I0(crcOut[13]),
        .I1(crcOut15_out),
        .I2(crcOut[0]),
        .I3(crcOut[14]),
        .O(\crcOut[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[14]_i_1 
       (.I0(crcOut[14]),
        .I1(crcOut15_out),
        .I2(crcOut[15]),
        .O(\crcOut[14]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \crcOut[15]_i_1 
       (.I0(p_1_in[0]),
        .I1(p_1_in[2]),
        .I2(\crcOut[15]_i_4_n_0 ),
        .O(\crcOut[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \crcOut[15]_i_2 
       (.I0(busyOut),
        .I1(crcOut15_out),
        .O(\crcOut[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[15]_i_3 
       (.I0(crcOut[15]),
        .I1(crcOut15_out),
        .I2(crcOut[0]),
        .O(\crcOut[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0001FFFFFFFFFFFF)) 
    \crcOut[15]_i_4 
       (.I0(s00_axi_wstrb[3]),
        .I1(s00_axi_wstrb[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_1_in[1]),
        .I5(S_AXI_WREADY),
        .O(\crcOut[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[1]_i_1 
       (.I0(s00_axi_wdata[1]),
        .I1(crcOut[1]),
        .I2(crcOut15_out),
        .I3(crcOut[2]),
        .O(\crcOut[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[2]_i_1 
       (.I0(s00_axi_wdata[2]),
        .I1(crcOut[2]),
        .I2(crcOut15_out),
        .I3(crcOut[3]),
        .O(\crcOut[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[3]_i_1 
       (.I0(s00_axi_wdata[3]),
        .I1(crcOut[3]),
        .I2(crcOut15_out),
        .I3(crcOut[4]),
        .O(\crcOut[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[4]_i_1 
       (.I0(s00_axi_wdata[4]),
        .I1(crcOut[4]),
        .I2(crcOut15_out),
        .I3(crcOut[5]),
        .O(\crcOut[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[5]_i_1 
       (.I0(s00_axi_wdata[5]),
        .I1(crcOut[5]),
        .I2(crcOut15_out),
        .I3(crcOut[6]),
        .O(\crcOut[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[6]_i_1 
       (.I0(s00_axi_wdata[6]),
        .I1(crcOut[6]),
        .I2(crcOut15_out),
        .I3(crcOut[7]),
        .O(\crcOut[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h6F60)) 
    \crcOut[7]_i_1 
       (.I0(s00_axi_wdata[7]),
        .I1(crcOut[7]),
        .I2(crcOut15_out),
        .I3(crcOut[8]),
        .O(\crcOut[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[8]_i_1 
       (.I0(crcOut[8]),
        .I1(crcOut15_out),
        .I2(crcOut[9]),
        .O(\crcOut[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[9]_i_1 
       (.I0(crcOut[9]),
        .I1(crcOut15_out),
        .I2(crcOut[10]),
        .O(\crcOut[9]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[0]_i_1_n_0 ),
        .Q(crcOut[0]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[10]_i_1_n_0 ),
        .Q(crcOut[10]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[11]_i_1_n_0 ),
        .Q(crcOut[11]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[12]_i_1_n_0 ),
        .Q(crcOut[12]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[13]_i_1_n_0 ),
        .Q(crcOut[13]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[14]_i_1_n_0 ),
        .Q(crcOut[14]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[15]_i_3_n_0 ),
        .Q(crcOut[15]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[1]_i_1_n_0 ),
        .Q(crcOut[1]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[2]_i_1_n_0 ),
        .Q(crcOut[2]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[3]_i_1_n_0 ),
        .Q(crcOut[3]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[4]_i_1_n_0 ),
        .Q(crcOut[4]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[5]_i_1_n_0 ),
        .Q(crcOut[5]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[6]_i_1_n_0 ),
        .Q(crcOut[6]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[7]_i_1_n_0 ),
        .Q(crcOut[7]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[8]_i_1_n_0 ),
        .Q(crcOut[8]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[9]_i_1_n_0 ),
        .Q(crcOut[9]),
        .S(\crcOut[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0006)) 
    \ctrIn[0]_i_1 
       (.I0(ctrIn[0]),
        .I1(busyIn),
        .I2(\crcIn[15]_i_1_n_0 ),
        .I3(crcIn19_out),
        .O(\ctrIn[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h0000006A)) 
    \ctrIn[1]_i_1 
       (.I0(ctrIn[1]),
        .I1(busyIn),
        .I2(ctrIn[0]),
        .I3(\crcIn[15]_i_1_n_0 ),
        .I4(crcIn19_out),
        .O(\ctrIn[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000006AAA)) 
    \ctrIn[2]_i_1 
       (.I0(ctrIn[2]),
        .I1(busyIn),
        .I2(ctrIn[0]),
        .I3(ctrIn[1]),
        .I4(\crcIn[15]_i_1_n_0 ),
        .I5(crcIn19_out),
        .O(\ctrIn[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[0]_i_1_n_0 ),
        .Q(ctrIn[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[1]_i_1_n_0 ),
        .Q(ctrIn[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[2]_i_1_n_0 ),
        .Q(ctrIn[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0006)) 
    \ctrOut[0]_i_1 
       (.I0(ctrOut[0]),
        .I1(busyOut),
        .I2(\crcOut[15]_i_1_n_0 ),
        .I3(crcOut15_out),
        .O(\ctrOut[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0000006A)) 
    \ctrOut[1]_i_1 
       (.I0(ctrOut[1]),
        .I1(busyOut),
        .I2(ctrOut[0]),
        .I3(\crcOut[15]_i_1_n_0 ),
        .I4(crcOut15_out),
        .O(\ctrOut[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000006AAA)) 
    \ctrOut[2]_i_1 
       (.I0(ctrOut[2]),
        .I1(busyOut),
        .I2(ctrOut[0]),
        .I3(ctrOut[1]),
        .I4(\crcOut[15]_i_1_n_0 ),
        .I5(crcOut15_out),
        .O(\ctrOut[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[0]_i_1_n_0 ),
        .Q(ctrOut[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[1]_i_1_n_0 ),
        .Q(ctrOut[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[2]_i_1_n_0 ),
        .Q(ctrOut[2]),
        .R(1'b0));
  CARRY4 localTimer0_carry
       (.CI(1'b0),
        .CO({localTimer0_carry_n_0,localTimer0_carry_n_1,localTimer0_carry_n_2,localTimer0_carry_n_3}),
        .CYINIT(localTimer[0]),
        .DI(localTimer[4:1]),
        .O(localTimer0[4:1]),
        .S({localTimer0_carry_i_1_n_0,localTimer0_carry_i_2_n_0,localTimer0_carry_i_3_n_0,localTimer0_carry_i_4_n_0}));
  CARRY4 localTimer0_carry__0
       (.CI(localTimer0_carry_n_0),
        .CO({localTimer0_carry__0_n_0,localTimer0_carry__0_n_1,localTimer0_carry__0_n_2,localTimer0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[8:5]),
        .O(localTimer0[8:5]),
        .S({localTimer0_carry__0_i_1_n_0,localTimer0_carry__0_i_2_n_0,localTimer0_carry__0_i_3_n_0,localTimer0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__0_i_1
       (.I0(localTimer[8]),
        .O(localTimer0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__0_i_2
       (.I0(localTimer[7]),
        .O(localTimer0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__0_i_3
       (.I0(localTimer[6]),
        .O(localTimer0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__0_i_4
       (.I0(localTimer[5]),
        .O(localTimer0_carry__0_i_4_n_0));
  CARRY4 localTimer0_carry__1
       (.CI(localTimer0_carry__0_n_0),
        .CO({localTimer0_carry__1_n_0,localTimer0_carry__1_n_1,localTimer0_carry__1_n_2,localTimer0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[12:9]),
        .O(localTimer0[12:9]),
        .S({localTimer0_carry__1_i_1_n_0,localTimer0_carry__1_i_2_n_0,localTimer0_carry__1_i_3_n_0,localTimer0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__1_i_1
       (.I0(localTimer[12]),
        .O(localTimer0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__1_i_2
       (.I0(localTimer[11]),
        .O(localTimer0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__1_i_3
       (.I0(localTimer[10]),
        .O(localTimer0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__1_i_4
       (.I0(localTimer[9]),
        .O(localTimer0_carry__1_i_4_n_0));
  CARRY4 localTimer0_carry__2
       (.CI(localTimer0_carry__1_n_0),
        .CO({localTimer0_carry__2_n_0,localTimer0_carry__2_n_1,localTimer0_carry__2_n_2,localTimer0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[16:13]),
        .O(localTimer0[16:13]),
        .S({localTimer0_carry__2_i_1_n_0,localTimer0_carry__2_i_2_n_0,localTimer0_carry__2_i_3_n_0,localTimer0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__2_i_1
       (.I0(localTimer[16]),
        .O(localTimer0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__2_i_2
       (.I0(localTimer[15]),
        .O(localTimer0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__2_i_3
       (.I0(localTimer[14]),
        .O(localTimer0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__2_i_4
       (.I0(localTimer[13]),
        .O(localTimer0_carry__2_i_4_n_0));
  CARRY4 localTimer0_carry__3
       (.CI(localTimer0_carry__2_n_0),
        .CO({localTimer0_carry__3_n_0,localTimer0_carry__3_n_1,localTimer0_carry__3_n_2,localTimer0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[20:17]),
        .O(localTimer0[20:17]),
        .S({localTimer0_carry__3_i_1_n_0,localTimer0_carry__3_i_2_n_0,localTimer0_carry__3_i_3_n_0,localTimer0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__3_i_1
       (.I0(localTimer[20]),
        .O(localTimer0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__3_i_2
       (.I0(localTimer[19]),
        .O(localTimer0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__3_i_3
       (.I0(localTimer[18]),
        .O(localTimer0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__3_i_4
       (.I0(localTimer[17]),
        .O(localTimer0_carry__3_i_4_n_0));
  CARRY4 localTimer0_carry__4
       (.CI(localTimer0_carry__3_n_0),
        .CO({localTimer0_carry__4_n_0,localTimer0_carry__4_n_1,localTimer0_carry__4_n_2,localTimer0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[24:21]),
        .O(localTimer0[24:21]),
        .S({localTimer0_carry__4_i_1_n_0,localTimer0_carry__4_i_2_n_0,localTimer0_carry__4_i_3_n_0,localTimer0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__4_i_1
       (.I0(localTimer[24]),
        .O(localTimer0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__4_i_2
       (.I0(localTimer[23]),
        .O(localTimer0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__4_i_3
       (.I0(localTimer[22]),
        .O(localTimer0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__4_i_4
       (.I0(localTimer[21]),
        .O(localTimer0_carry__4_i_4_n_0));
  CARRY4 localTimer0_carry__5
       (.CI(localTimer0_carry__4_n_0),
        .CO({localTimer0_carry__5_n_0,localTimer0_carry__5_n_1,localTimer0_carry__5_n_2,localTimer0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(localTimer[28:25]),
        .O(localTimer0[28:25]),
        .S({localTimer0_carry__5_i_1_n_0,localTimer0_carry__5_i_2_n_0,localTimer0_carry__5_i_3_n_0,localTimer0_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__5_i_1
       (.I0(localTimer[28]),
        .O(localTimer0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__5_i_2
       (.I0(localTimer[27]),
        .O(localTimer0_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__5_i_3
       (.I0(localTimer[26]),
        .O(localTimer0_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__5_i_4
       (.I0(localTimer[25]),
        .O(localTimer0_carry__5_i_4_n_0));
  CARRY4 localTimer0_carry__6
       (.CI(localTimer0_carry__5_n_0),
        .CO({NLW_localTimer0_carry__6_CO_UNCONNECTED[3:2],localTimer0_carry__6_n_2,localTimer0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,localTimer[30:29]}),
        .O({NLW_localTimer0_carry__6_O_UNCONNECTED[3],localTimer0[31:29]}),
        .S({1'b0,localTimer0_carry__6_i_1_n_0,localTimer0_carry__6_i_2_n_0,localTimer0_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__6_i_1
       (.I0(localTimer[31]),
        .O(localTimer0_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__6_i_2
       (.I0(localTimer[30]),
        .O(localTimer0_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry__6_i_3
       (.I0(localTimer[29]),
        .O(localTimer0_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry_i_1
       (.I0(localTimer[4]),
        .O(localTimer0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry_i_2
       (.I0(localTimer[3]),
        .O(localTimer0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry_i_3
       (.I0(localTimer[2]),
        .O(localTimer0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    localTimer0_carry_i_4
       (.I0(localTimer[1]),
        .O(localTimer0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h8ABA)) 
    \localTimer[0]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(\localTimer[31]_i_7_n_0 ),
        .I3(localTimer[0]),
        .O(\localTimer[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[10]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[10]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[10]),
        .O(\localTimer[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[11]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[11]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[11]),
        .O(\localTimer[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[12]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[12]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[12]),
        .O(\localTimer[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[13]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[13]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[13]),
        .O(\localTimer[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[14]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[14]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[14]),
        .O(\localTimer[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDD0D0000)) 
    \localTimer[15]_i_1 
       (.I0(\localTimer[31]_i_3_n_0 ),
        .I1(\localTimer[31]_i_4_n_0 ),
        .I2(\localTimer[31]_i_5_n_0 ),
        .I3(\localTimer[31]_i_6_n_0 ),
        .I4(\localTimer[31]_i_7_n_0 ),
        .I5(\localTimer[15]_i_3_n_0 ),
        .O(\localTimer[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[15]_i_2 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[15]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[15]),
        .O(\localTimer[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \localTimer[15]_i_3 
       (.I0(\localTimer[31]_i_9_n_0 ),
        .I1(\crcOut[15]_i_4_n_0 ),
        .I2(p_1_in[0]),
        .I3(p_1_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\localTimer[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[16]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[16]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[16]),
        .O(\localTimer[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[17]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[17]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[17]),
        .O(\localTimer[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[18]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[18]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[18]),
        .O(\localTimer[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[19]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[19]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[19]),
        .O(\localTimer[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[1]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[1]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[1]),
        .O(\localTimer[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[20]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[20]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[20]),
        .O(\localTimer[20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[21]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[21]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[21]),
        .O(\localTimer[21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[22]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[22]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[22]),
        .O(\localTimer[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDD0D0000)) 
    \localTimer[23]_i_1 
       (.I0(\localTimer[31]_i_3_n_0 ),
        .I1(\localTimer[31]_i_4_n_0 ),
        .I2(\localTimer[31]_i_5_n_0 ),
        .I3(\localTimer[31]_i_6_n_0 ),
        .I4(\localTimer[31]_i_7_n_0 ),
        .I5(\localTimer[23]_i_3_n_0 ),
        .O(\localTimer[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[23]_i_2 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[23]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[23]),
        .O(\localTimer[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \localTimer[23]_i_3 
       (.I0(\localTimer[31]_i_9_n_0 ),
        .I1(\crcOut[15]_i_4_n_0 ),
        .I2(p_1_in[0]),
        .I3(p_1_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\localTimer[23]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[24]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[24]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[24]),
        .O(\localTimer[24]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[25]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[25]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[25]),
        .O(\localTimer[25]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[26]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[26]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[26]),
        .O(\localTimer[26]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[27]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[27]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[27]),
        .O(\localTimer[27]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[28]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[28]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[28]),
        .O(\localTimer[28]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[29]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[29]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[29]),
        .O(\localTimer[29]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[2]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[2]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[2]),
        .O(\localTimer[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[30]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[30]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[30]),
        .O(\localTimer[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDD0D0000)) 
    \localTimer[31]_i_1 
       (.I0(\localTimer[31]_i_3_n_0 ),
        .I1(\localTimer[31]_i_4_n_0 ),
        .I2(\localTimer[31]_i_5_n_0 ),
        .I3(\localTimer[31]_i_6_n_0 ),
        .I4(\localTimer[31]_i_7_n_0 ),
        .I5(\localTimer[31]_i_8_n_0 ),
        .O(\localTimer[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_10 
       (.I0(localTimer[2]),
        .I1(localTimer[3]),
        .I2(localTimer[0]),
        .I3(localTimer[1]),
        .O(\localTimer[31]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \localTimer[31]_i_11 
       (.I0(localTimer[4]),
        .I1(localTimer[5]),
        .I2(localTimer[6]),
        .I3(localTimer[7]),
        .O(\localTimer[31]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_12 
       (.I0(localTimer[12]),
        .I1(localTimer[13]),
        .I2(localTimer[14]),
        .I3(localTimer[15]),
        .O(\localTimer[31]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_13 
       (.I0(localTimer[9]),
        .I1(localTimer[10]),
        .I2(localTimer[8]),
        .I3(localTimer[11]),
        .O(\localTimer[31]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_14 
       (.I0(localTimer[29]),
        .I1(localTimer[30]),
        .I2(localTimer[28]),
        .I3(localTimer[31]),
        .O(\localTimer[31]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_15 
       (.I0(localTimer[25]),
        .I1(localTimer[26]),
        .I2(localTimer[24]),
        .I3(localTimer[27]),
        .O(\localTimer[31]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_16 
       (.I0(localTimer[16]),
        .I1(localTimer[17]),
        .I2(localTimer[18]),
        .I3(localTimer[19]),
        .O(\localTimer[31]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_17 
       (.I0(localTimer[20]),
        .I1(localTimer[23]),
        .I2(localTimer[21]),
        .I3(localTimer[22]),
        .O(\localTimer[31]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_18 
       (.I0(localTimer[0]),
        .I1(localTimer[1]),
        .I2(localTimer[2]),
        .I3(localTimer[3]),
        .O(\localTimer[31]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \localTimer[31]_i_19 
       (.I0(localTimer[6]),
        .I1(localTimer[7]),
        .I2(localTimer[4]),
        .I3(localTimer[5]),
        .O(\localTimer[31]_i_19_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[31]_i_2 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[31]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[31]),
        .O(\localTimer[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_20 
       (.I0(localTimer[13]),
        .I1(localTimer[14]),
        .I2(localTimer[12]),
        .I3(localTimer[15]),
        .O(\localTimer[31]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_21 
       (.I0(localTimer[8]),
        .I1(localTimer[11]),
        .I2(localTimer[9]),
        .I3(localTimer[10]),
        .O(\localTimer[31]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_22 
       (.I0(localTimer[18]),
        .I1(localTimer[19]),
        .I2(localTimer[16]),
        .I3(localTimer[17]),
        .O(\localTimer[31]_i_22_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_23 
       (.I0(localTimer[20]),
        .I1(localTimer[23]),
        .I2(localTimer[21]),
        .I3(localTimer[22]),
        .O(\localTimer[31]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_24 
       (.I0(localTimer[30]),
        .I1(localTimer[31]),
        .I2(localTimer[28]),
        .I3(localTimer[29]),
        .O(\localTimer[31]_i_24_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[31]_i_25 
       (.I0(localTimer[25]),
        .I1(localTimer[26]),
        .I2(localTimer[24]),
        .I3(localTimer[27]),
        .O(\localTimer[31]_i_25_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \localTimer[31]_i_3 
       (.I0(\localTimer[31]_i_10_n_0 ),
        .I1(\localTimer[31]_i_11_n_0 ),
        .I2(\localTimer[31]_i_12_n_0 ),
        .I3(\localTimer[31]_i_13_n_0 ),
        .O(\localTimer[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_4 
       (.I0(\localTimer[31]_i_14_n_0 ),
        .I1(\localTimer[31]_i_15_n_0 ),
        .I2(\localTimer[31]_i_16_n_0 ),
        .I3(\localTimer[31]_i_17_n_0 ),
        .O(\localTimer[31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \localTimer[31]_i_5 
       (.I0(\localTimer[31]_i_18_n_0 ),
        .I1(\localTimer[31]_i_19_n_0 ),
        .I2(\localTimer[31]_i_20_n_0 ),
        .I3(\localTimer[31]_i_21_n_0 ),
        .O(\localTimer[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \localTimer[31]_i_6 
       (.I0(\localTimer[31]_i_22_n_0 ),
        .I1(\localTimer[31]_i_23_n_0 ),
        .I2(\localTimer[31]_i_24_n_0 ),
        .I3(\localTimer[31]_i_25_n_0 ),
        .O(\localTimer[31]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \localTimer[31]_i_7 
       (.I0(\crcOut[15]_i_4_n_0 ),
        .I1(p_1_in[0]),
        .I2(p_1_in[2]),
        .O(\localTimer[31]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \localTimer[31]_i_8 
       (.I0(\localTimer[31]_i_9_n_0 ),
        .I1(\crcOut[15]_i_4_n_0 ),
        .I2(p_1_in[0]),
        .I3(p_1_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\localTimer[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \localTimer[31]_i_9 
       (.I0(p_1_in[0]),
        .I1(p_1_in[2]),
        .I2(p_1_in[1]),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_wstrb[0]),
        .O(\localTimer[31]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[3]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[3]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[3]),
        .O(\localTimer[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[4]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[4]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[4]),
        .O(\localTimer[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[5]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[5]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[5]),
        .O(\localTimer[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[6]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[6]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[6]),
        .O(\localTimer[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDD0D0000FFFFFFFF)) 
    \localTimer[7]_i_1 
       (.I0(\localTimer[31]_i_3_n_0 ),
        .I1(\localTimer[31]_i_4_n_0 ),
        .I2(\localTimer[31]_i_5_n_0 ),
        .I3(\localTimer[31]_i_6_n_0 ),
        .I4(\localTimer[31]_i_7_n_0 ),
        .I5(\localTimer[7]_i_3_n_0 ),
        .O(\localTimer[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[7]_i_2 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[7]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[7]),
        .O(\localTimer[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \localTimer[7]_i_3 
       (.I0(s00_axi_wstrb[0]),
        .I1(S_AXI_WREADY),
        .I2(p_1_in[1]),
        .I3(p_1_in[2]),
        .O(\localTimer[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[8]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[8]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[8]),
        .O(\localTimer[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \localTimer[9]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(localTimer0[9]),
        .I3(\localTimer[31]_i_7_n_0 ),
        .I4(s00_axi_wdata[9]),
        .O(\localTimer[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[0]_i_1_n_0 ),
        .Q(localTimer[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[10]_i_1_n_0 ),
        .Q(localTimer[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[11]_i_1_n_0 ),
        .Q(localTimer[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[12]_i_1_n_0 ),
        .Q(localTimer[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[13]_i_1_n_0 ),
        .Q(localTimer[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[14]_i_1_n_0 ),
        .Q(localTimer[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[15]_i_2_n_0 ),
        .Q(localTimer[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[16]_i_1_n_0 ),
        .Q(localTimer[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[17]_i_1_n_0 ),
        .Q(localTimer[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[18]_i_1_n_0 ),
        .Q(localTimer[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[19]_i_1_n_0 ),
        .Q(localTimer[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[1]_i_1_n_0 ),
        .Q(localTimer[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[20]_i_1_n_0 ),
        .Q(localTimer[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[21]_i_1_n_0 ),
        .Q(localTimer[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[22]_i_1_n_0 ),
        .Q(localTimer[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[23]_i_1_n_0 ),
        .D(\localTimer[23]_i_2_n_0 ),
        .Q(localTimer[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[24]_i_1_n_0 ),
        .Q(localTimer[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[25]_i_1_n_0 ),
        .Q(localTimer[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[26]_i_1_n_0 ),
        .Q(localTimer[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[27]_i_1_n_0 ),
        .Q(localTimer[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[28]_i_1_n_0 ),
        .Q(localTimer[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[29]_i_1_n_0 ),
        .Q(localTimer[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[2]_i_1_n_0 ),
        .Q(localTimer[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[30]_i_1_n_0 ),
        .Q(localTimer[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[31]_i_1_n_0 ),
        .D(\localTimer[31]_i_2_n_0 ),
        .Q(localTimer[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[3]_i_1_n_0 ),
        .Q(localTimer[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[4]_i_1_n_0 ),
        .Q(localTimer[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[5]_i_1_n_0 ),
        .Q(localTimer[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[6]_i_1_n_0 ),
        .Q(localTimer[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[7]_i_1_n_0 ),
        .D(\localTimer[7]_i_2_n_0 ),
        .Q(localTimer[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[8]_i_1_n_0 ),
        .Q(localTimer[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \localTimer_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\localTimer[15]_i_1_n_0 ),
        .D(\localTimer[9]_i_1_n_0 ),
        .Q(localTimer[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBBBBBB8BBBBBBBB)) 
    local_i_1
       (.I0(s00_axi_wdata[0]),
        .I1(\localTimer[31]_i_9_n_0 ),
        .I2(local_i_2_n_0),
        .I3(local_i_3_n_0),
        .I4(local_i_4_n_0),
        .I5(local_i_5_n_0),
        .O(local_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    local_i_2
       (.I0(localTimer[22]),
        .I1(localTimer[21]),
        .I2(localTimer[23]),
        .I3(localTimer[20]),
        .I4(\localTimer[31]_i_16_n_0 ),
        .O(local_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    local_i_3
       (.I0(localTimer[27]),
        .I1(localTimer[24]),
        .I2(localTimer[26]),
        .I3(localTimer[25]),
        .I4(\localTimer[31]_i_14_n_0 ),
        .O(local_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    local_i_4
       (.I0(localTimer[11]),
        .I1(localTimer[8]),
        .I2(localTimer[10]),
        .I3(localTimer[9]),
        .I4(\localTimer[31]_i_12_n_0 ),
        .O(local_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    local_i_5
       (.I0(localTimer[7]),
        .I1(localTimer[6]),
        .I2(localTimer[5]),
        .I3(localTimer[4]),
        .I4(\localTimer[31]_i_10_n_0 ),
        .O(local_i_5_n_0));
  FDRE #(
    .INIT(1'b1)) 
    local_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(local_i_1_n_0),
        .Q(local),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFE)) 
    \slaveAddr[7]_i_1 
       (.I0(\slaveAddr[7]_i_2_n_0 ),
        .I1(s00_axi_wdata[5]),
        .I2(s00_axi_wdata[2]),
        .I3(s00_axi_wdata[4]),
        .I4(s00_axi_wdata[1]),
        .I5(\slaveAddr[7]_i_3_n_0 ),
        .O(slaveAddr0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \slaveAddr[7]_i_2 
       (.I0(s00_axi_wdata[3]),
        .I1(s00_axi_wdata[0]),
        .I2(s00_axi_wdata[7]),
        .I3(s00_axi_wdata[6]),
        .O(\slaveAddr[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \slaveAddr[7]_i_3 
       (.I0(\slaveAddr[7]_i_4_n_0 ),
        .I1(p_1_in[2]),
        .I2(p_1_in[0]),
        .I3(p_1_in[1]),
        .I4(S_AXI_WREADY),
        .O(\slaveAddr[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slaveAddr[7]_i_4 
       (.I0(s00_axi_wdata[7]),
        .I1(s00_axi_wdata[4]),
        .I2(s00_axi_wdata[3]),
        .I3(s00_axi_wdata[5]),
        .I4(s00_axi_wdata[6]),
        .O(\slaveAddr[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slaveAddr_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[0]),
        .Q(slaveAddr[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[1]),
        .Q(slaveAddr[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[2]),
        .Q(slaveAddr[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[3]),
        .Q(slaveAddr[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[4]),
        .Q(slaveAddr[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[5]),
        .Q(slaveAddr[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[6]),
        .Q(slaveAddr[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[7]),
        .Q(slaveAddr[7]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h40)) 
    slv_reg_rden
       (.I0(s00_axi_rvalid),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .O(slv_reg_rden__0));
  LUT3 #(
    .INIT(8'h08)) 
    \timer[0]_i_1 
       (.I0(\crcIn[15]_i_4_n_0 ),
        .I1(p_1_in[2]),
        .I2(p_1_in[0]),
        .O(timer0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_10 
       (.I0(timer_reg[29]),
        .I1(timer_reg[30]),
        .I2(timer_reg[24]),
        .I3(timer_reg[25]),
        .O(\timer[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_11 
       (.I0(timer_reg[11]),
        .I1(timer_reg[7]),
        .I2(timer_reg[12]),
        .I3(timer_reg[15]),
        .O(\timer[0]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_12 
       (.I0(timer_reg[10]),
        .I1(timer_reg[0]),
        .I2(timer_reg[17]),
        .I3(timer_reg[22]),
        .O(\timer[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \timer[0]_i_2 
       (.I0(\timer[0]_i_4_n_0 ),
        .I1(\timer[0]_i_5_n_0 ),
        .I2(\timer[0]_i_6_n_0 ),
        .I3(\timer[0]_i_7_n_0 ),
        .O(sel));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \timer[0]_i_4 
       (.I0(timer_reg[19]),
        .I1(timer_reg[16]),
        .I2(timer_reg[21]),
        .I3(timer_reg[18]),
        .I4(\timer[0]_i_9_n_0 ),
        .O(\timer[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \timer[0]_i_5 
       (.I0(timer_reg[4]),
        .I1(timer_reg[13]),
        .I2(timer_reg[3]),
        .I3(timer_reg[2]),
        .I4(\timer[0]_i_10_n_0 ),
        .O(\timer[0]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \timer[0]_i_6 
       (.I0(timer_reg[31]),
        .I1(timer_reg[28]),
        .I2(timer_reg[9]),
        .I3(timer_reg[8]),
        .I4(\timer[0]_i_11_n_0 ),
        .O(\timer[0]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \timer[0]_i_7 
       (.I0(timer_reg[27]),
        .I1(timer_reg[26]),
        .I2(timer_reg[6]),
        .I3(timer_reg[5]),
        .I4(\timer[0]_i_12_n_0 ),
        .O(\timer[0]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \timer[0]_i_8 
       (.I0(timer_reg[0]),
        .O(\timer[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_9 
       (.I0(timer_reg[14]),
        .I1(timer_reg[1]),
        .I2(timer_reg[20]),
        .I3(timer_reg[23]),
        .O(\timer[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[0] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_7 ),
        .Q(timer_reg[0]),
        .R(timer0));
  CARRY4 \timer_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\timer_reg[0]_i_3_n_0 ,\timer_reg[0]_i_3_n_1 ,\timer_reg[0]_i_3_n_2 ,\timer_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\timer_reg[0]_i_3_n_4 ,\timer_reg[0]_i_3_n_5 ,\timer_reg[0]_i_3_n_6 ,\timer_reg[0]_i_3_n_7 }),
        .S({timer_reg[3:1],\timer[0]_i_8_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[10] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_5 ),
        .Q(timer_reg[10]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[11] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_4 ),
        .Q(timer_reg[11]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[12] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_7 ),
        .Q(timer_reg[12]),
        .R(timer0));
  CARRY4 \timer_reg[12]_i_1 
       (.CI(\timer_reg[8]_i_1_n_0 ),
        .CO({\timer_reg[12]_i_1_n_0 ,\timer_reg[12]_i_1_n_1 ,\timer_reg[12]_i_1_n_2 ,\timer_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[12]_i_1_n_4 ,\timer_reg[12]_i_1_n_5 ,\timer_reg[12]_i_1_n_6 ,\timer_reg[12]_i_1_n_7 }),
        .S(timer_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[13] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_6 ),
        .Q(timer_reg[13]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[14] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_5 ),
        .Q(timer_reg[14]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[15] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_4 ),
        .Q(timer_reg[15]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[16] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_7 ),
        .Q(timer_reg[16]),
        .R(timer0));
  CARRY4 \timer_reg[16]_i_1 
       (.CI(\timer_reg[12]_i_1_n_0 ),
        .CO({\timer_reg[16]_i_1_n_0 ,\timer_reg[16]_i_1_n_1 ,\timer_reg[16]_i_1_n_2 ,\timer_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[16]_i_1_n_4 ,\timer_reg[16]_i_1_n_5 ,\timer_reg[16]_i_1_n_6 ,\timer_reg[16]_i_1_n_7 }),
        .S(timer_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[17] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_6 ),
        .Q(timer_reg[17]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[18] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_5 ),
        .Q(timer_reg[18]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[19] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_4 ),
        .Q(timer_reg[19]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[1] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_6 ),
        .Q(timer_reg[1]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[20] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_7 ),
        .Q(timer_reg[20]),
        .R(timer0));
  CARRY4 \timer_reg[20]_i_1 
       (.CI(\timer_reg[16]_i_1_n_0 ),
        .CO({\timer_reg[20]_i_1_n_0 ,\timer_reg[20]_i_1_n_1 ,\timer_reg[20]_i_1_n_2 ,\timer_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[20]_i_1_n_4 ,\timer_reg[20]_i_1_n_5 ,\timer_reg[20]_i_1_n_6 ,\timer_reg[20]_i_1_n_7 }),
        .S(timer_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[21] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_6 ),
        .Q(timer_reg[21]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[22] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_5 ),
        .Q(timer_reg[22]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[23] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_4 ),
        .Q(timer_reg[23]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[24] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_7 ),
        .Q(timer_reg[24]),
        .R(timer0));
  CARRY4 \timer_reg[24]_i_1 
       (.CI(\timer_reg[20]_i_1_n_0 ),
        .CO({\timer_reg[24]_i_1_n_0 ,\timer_reg[24]_i_1_n_1 ,\timer_reg[24]_i_1_n_2 ,\timer_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[24]_i_1_n_4 ,\timer_reg[24]_i_1_n_5 ,\timer_reg[24]_i_1_n_6 ,\timer_reg[24]_i_1_n_7 }),
        .S(timer_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[25] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_6 ),
        .Q(timer_reg[25]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[26] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_5 ),
        .Q(timer_reg[26]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[27] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_4 ),
        .Q(timer_reg[27]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[28] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_7 ),
        .Q(timer_reg[28]),
        .R(timer0));
  CARRY4 \timer_reg[28]_i_1 
       (.CI(\timer_reg[24]_i_1_n_0 ),
        .CO({\NLW_timer_reg[28]_i_1_CO_UNCONNECTED [3],\timer_reg[28]_i_1_n_1 ,\timer_reg[28]_i_1_n_2 ,\timer_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[28]_i_1_n_4 ,\timer_reg[28]_i_1_n_5 ,\timer_reg[28]_i_1_n_6 ,\timer_reg[28]_i_1_n_7 }),
        .S(timer_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[29] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_6 ),
        .Q(timer_reg[29]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[2] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_5 ),
        .Q(timer_reg[2]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[30] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_5 ),
        .Q(timer_reg[30]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[31] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_4 ),
        .Q(timer_reg[31]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[3] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_4 ),
        .Q(timer_reg[3]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[4] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_7 ),
        .Q(timer_reg[4]),
        .R(timer0));
  CARRY4 \timer_reg[4]_i_1 
       (.CI(\timer_reg[0]_i_3_n_0 ),
        .CO({\timer_reg[4]_i_1_n_0 ,\timer_reg[4]_i_1_n_1 ,\timer_reg[4]_i_1_n_2 ,\timer_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[4]_i_1_n_4 ,\timer_reg[4]_i_1_n_5 ,\timer_reg[4]_i_1_n_6 ,\timer_reg[4]_i_1_n_7 }),
        .S(timer_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[5] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_6 ),
        .Q(timer_reg[5]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[6] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_5 ),
        .Q(timer_reg[6]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[7] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_4 ),
        .Q(timer_reg[7]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[8] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_7 ),
        .Q(timer_reg[8]),
        .R(timer0));
  CARRY4 \timer_reg[8]_i_1 
       (.CI(\timer_reg[4]_i_1_n_0 ),
        .CO({\timer_reg[8]_i_1_n_0 ,\timer_reg[8]_i_1_n_1 ,\timer_reg[8]_i_1_n_2 ,\timer_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[8]_i_1_n_4 ,\timer_reg[8]_i_1_n_5 ,\timer_reg[8]_i_1_n_6 ,\timer_reg[8]_i_1_n_7 }),
        .S(timer_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[9] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_6 ),
        .Q(timer_reg[9]),
        .R(timer0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
