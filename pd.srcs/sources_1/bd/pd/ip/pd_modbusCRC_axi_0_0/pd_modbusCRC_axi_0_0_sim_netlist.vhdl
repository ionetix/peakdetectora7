-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
-- Date        : Thu Jan 16 08:20:34 2020
-- Host        : Windows10 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               Z:/Documents/git/master/pd/pd.srcs/sources_1/bd/pd/ip/pd_modbusCRC_axi_0_0/pd_modbusCRC_axi_0_0_sim_netlist.vhdl
-- Design      : pd_modbusCRC_axi_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a15tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI is
  port (
    S_AXI_ARREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI : entity is "modbusCRC_axi_v1_0_S00_AXI";
end pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI;

architecture STRUCTURE of pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready_i_1_n_0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal busyIn : STD_LOGIC;
  signal busyIn_i_1_n_0 : STD_LOGIC;
  signal busyIn_i_3_n_0 : STD_LOGIC;
  signal busyOut : STD_LOGIC;
  signal busyOut_i_1_n_0 : STD_LOGIC;
  signal crcIn : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal crcIn19_out : STD_LOGIC;
  signal \crcIn[0]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[10]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[11]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[12]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[13]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[14]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[15]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[15]_i_2_n_0\ : STD_LOGIC;
  signal \crcIn[15]_i_3_n_0\ : STD_LOGIC;
  signal \crcIn[15]_i_4_n_0\ : STD_LOGIC;
  signal \crcIn[1]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[2]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[3]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[4]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[5]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[6]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[7]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[8]_i_1_n_0\ : STD_LOGIC;
  signal \crcIn[9]_i_1_n_0\ : STD_LOGIC;
  signal crcOut : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal crcOut15_out : STD_LOGIC;
  signal \crcOut[0]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[10]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[11]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[12]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[13]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[14]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[15]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[15]_i_2_n_0\ : STD_LOGIC;
  signal \crcOut[15]_i_3_n_0\ : STD_LOGIC;
  signal \crcOut[15]_i_4_n_0\ : STD_LOGIC;
  signal \crcOut[1]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[2]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[3]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[4]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[5]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[6]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[7]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[8]_i_1_n_0\ : STD_LOGIC;
  signal \crcOut[9]_i_1_n_0\ : STD_LOGIC;
  signal ctrIn : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \ctrIn[0]_i_1_n_0\ : STD_LOGIC;
  signal \ctrIn[1]_i_1_n_0\ : STD_LOGIC;
  signal \ctrIn[2]_i_1_n_0\ : STD_LOGIC;
  signal ctrOut : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \ctrOut[0]_i_1_n_0\ : STD_LOGIC;
  signal \ctrOut[1]_i_1_n_0\ : STD_LOGIC;
  signal \ctrOut[2]_i_1_n_0\ : STD_LOGIC;
  signal local : STD_LOGIC;
  signal localTimer : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal localTimer0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \localTimer0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__0_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__0_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__0_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__0_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__1_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__1_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__1_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__2_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__2_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__2_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__3_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__3_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__3_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__4_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__4_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__4_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__5_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__5_n_1\ : STD_LOGIC;
  signal \localTimer0_carry__5_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__5_n_3\ : STD_LOGIC;
  signal \localTimer0_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \localTimer0_carry__6_n_2\ : STD_LOGIC;
  signal \localTimer0_carry__6_n_3\ : STD_LOGIC;
  signal localTimer0_carry_i_1_n_0 : STD_LOGIC;
  signal localTimer0_carry_i_2_n_0 : STD_LOGIC;
  signal localTimer0_carry_i_3_n_0 : STD_LOGIC;
  signal localTimer0_carry_i_4_n_0 : STD_LOGIC;
  signal localTimer0_carry_n_0 : STD_LOGIC;
  signal localTimer0_carry_n_1 : STD_LOGIC;
  signal localTimer0_carry_n_2 : STD_LOGIC;
  signal localTimer0_carry_n_3 : STD_LOGIC;
  signal \localTimer[0]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[10]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[11]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[12]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[13]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[14]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[15]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[15]_i_2_n_0\ : STD_LOGIC;
  signal \localTimer[15]_i_3_n_0\ : STD_LOGIC;
  signal \localTimer[16]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[17]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[18]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[19]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[1]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[20]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[21]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[22]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[23]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[23]_i_2_n_0\ : STD_LOGIC;
  signal \localTimer[23]_i_3_n_0\ : STD_LOGIC;
  signal \localTimer[24]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[25]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[26]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[27]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[28]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[29]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[2]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[30]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_10_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_11_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_12_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_13_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_14_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_15_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_16_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_17_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_18_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_19_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_20_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_21_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_22_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_23_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_24_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_25_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_2_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_3_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_4_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_5_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_6_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_7_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_8_n_0\ : STD_LOGIC;
  signal \localTimer[31]_i_9_n_0\ : STD_LOGIC;
  signal \localTimer[3]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[4]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[5]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[6]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[7]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[7]_i_2_n_0\ : STD_LOGIC;
  signal \localTimer[7]_i_3_n_0\ : STD_LOGIC;
  signal \localTimer[8]_i_1_n_0\ : STD_LOGIC;
  signal \localTimer[9]_i_1_n_0\ : STD_LOGIC;
  signal local_i_1_n_0 : STD_LOGIC;
  signal local_i_2_n_0 : STD_LOGIC;
  signal local_i_3_n_0 : STD_LOGIC;
  signal local_i_4_n_0 : STD_LOGIC;
  signal local_i_5_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slaveAddr : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal slaveAddr0 : STD_LOGIC;
  signal \slaveAddr[7]_i_2_n_0\ : STD_LOGIC;
  signal \slaveAddr[7]_i_3_n_0\ : STD_LOGIC;
  signal \slaveAddr[7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal timer0 : STD_LOGIC;
  signal \timer[0]_i_10_n_0\ : STD_LOGIC;
  signal \timer[0]_i_11_n_0\ : STD_LOGIC;
  signal \timer[0]_i_12_n_0\ : STD_LOGIC;
  signal \timer[0]_i_4_n_0\ : STD_LOGIC;
  signal \timer[0]_i_5_n_0\ : STD_LOGIC;
  signal \timer[0]_i_6_n_0\ : STD_LOGIC;
  signal \timer[0]_i_7_n_0\ : STD_LOGIC;
  signal \timer[0]_i_8_n_0\ : STD_LOGIC;
  signal \timer[0]_i_9_n_0\ : STD_LOGIC;
  signal timer_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \timer_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \timer_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \timer_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_localTimer0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_localTimer0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_timer_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_araddr[3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \axi_awaddr[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of busyIn_i_2 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of busyOut_i_2 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \crcIn[0]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \crcIn[10]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \crcIn[11]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \crcIn[12]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \crcIn[13]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \crcIn[14]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \crcIn[15]_i_3\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \crcIn[7]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \crcIn[8]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \crcIn[9]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \crcOut[0]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \crcOut[10]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \crcOut[11]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \crcOut[12]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \crcOut[13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \crcOut[14]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \crcOut[15]_i_3\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \crcOut[7]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \crcOut[8]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \crcOut[9]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \ctrIn[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ctrIn[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ctrOut[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ctrOut[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \localTimer[31]_i_10\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \localTimer[31]_i_11\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \localTimer[31]_i_12\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \localTimer[31]_i_13\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \localTimer[31]_i_14\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \localTimer[31]_i_15\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \localTimer[31]_i_16\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \localTimer[31]_i_17\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \localTimer[31]_i_18\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \localTimer[31]_i_20\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \localTimer[31]_i_22\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \localTimer[31]_i_24\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of local_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of local_i_3 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of local_i_4 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of local_i_5 : label is "soft_lutpair5";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(2),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => sel0(2),
      O => \axi_araddr[4]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => sel0(0),
      R => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => sel0(1),
      R => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[4]_i_1_n_0\,
      Q => sel0(2),
      R => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready_i_1_n_0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready_i_1_n_0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      I4 => p_1_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      I4 => p_1_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s00_axi_awaddr(2),
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s_axi_awready\,
      I4 => p_1_in(2),
      O => \axi_awaddr[4]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_1_in(0),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_1_in(1),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[4]_i_1_n_0\,
      Q => p_1_in(2),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7444444444444444"
    )
        port map (
      I0 => s00_axi_bready,
      I1 => \^s00_axi_bvalid\,
      I2 => \^s_axi_awready\,
      I3 => \^s_axi_wready\,
      I4 => s00_axi_awvalid,
      I5 => s00_axi_wvalid,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(0),
      I3 => sel0(1),
      I4 => crcIn(0),
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => localTimer(0),
      I1 => local,
      I2 => sel0(1),
      I3 => slaveAddr(0),
      I4 => sel0(0),
      I5 => timer_reg(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(10),
      I3 => sel0(1),
      I4 => crcIn(10),
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(10),
      I1 => sel0(1),
      I2 => timer_reg(10),
      I3 => sel0(0),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(11),
      I3 => sel0(1),
      I4 => crcIn(11),
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(11),
      I1 => sel0(1),
      I2 => timer_reg(11),
      I3 => sel0(0),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(12),
      I3 => sel0(1),
      I4 => crcIn(12),
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(12),
      I1 => sel0(1),
      I2 => timer_reg(12),
      I3 => sel0(0),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(13),
      I3 => sel0(1),
      I4 => crcIn(13),
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(13),
      I1 => sel0(1),
      I2 => timer_reg(13),
      I3 => sel0(0),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(14),
      I3 => sel0(1),
      I4 => crcIn(14),
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(14),
      I1 => sel0(1),
      I2 => timer_reg(14),
      I3 => sel0(0),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(15),
      I3 => sel0(1),
      I4 => crcIn(15),
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(15),
      I1 => sel0(1),
      I2 => timer_reg(15),
      I3 => sel0(0),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(16),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(16),
      O => reg_data_out(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(17),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(17),
      O => reg_data_out(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(18),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(18),
      O => reg_data_out(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(19),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(19),
      O => reg_data_out(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(1),
      I3 => sel0(1),
      I4 => crcIn(1),
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(1),
      I1 => sel0(1),
      I2 => slaveAddr(1),
      I3 => sel0(0),
      I4 => timer_reg(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(20),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(20),
      O => reg_data_out(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(21),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(21),
      O => reg_data_out(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(22),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(22),
      O => reg_data_out(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(23),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(23),
      O => reg_data_out(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(24),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(24),
      O => reg_data_out(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(25),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(25),
      O => reg_data_out(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(26),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(26),
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(27),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(27),
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(28),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(28),
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(29),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(29),
      O => reg_data_out(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(2),
      I3 => sel0(1),
      I4 => crcIn(2),
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(2),
      I1 => sel0(1),
      I2 => slaveAddr(2),
      I3 => sel0(0),
      I4 => timer_reg(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(30),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(30),
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0080008"
    )
        port map (
      I0 => sel0(2),
      I1 => timer_reg(31),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => localTimer(31),
      O => reg_data_out(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(3),
      I3 => sel0(1),
      I4 => crcIn(3),
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(3),
      I1 => sel0(1),
      I2 => slaveAddr(3),
      I3 => sel0(0),
      I4 => timer_reg(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(4),
      I3 => sel0(1),
      I4 => crcIn(4),
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(4),
      I1 => sel0(1),
      I2 => slaveAddr(4),
      I3 => sel0(0),
      I4 => timer_reg(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(5),
      I3 => sel0(1),
      I4 => crcIn(5),
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(5),
      I1 => sel0(1),
      I2 => slaveAddr(5),
      I3 => sel0(0),
      I4 => timer_reg(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(6),
      I3 => sel0(1),
      I4 => crcIn(6),
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(6),
      I1 => sel0(1),
      I2 => slaveAddr(6),
      I3 => sel0(0),
      I4 => timer_reg(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(7),
      I3 => sel0(1),
      I4 => crcIn(7),
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => localTimer(7),
      I1 => sel0(1),
      I2 => slaveAddr(7),
      I3 => sel0(0),
      I4 => timer_reg(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(8),
      I3 => sel0(1),
      I4 => crcIn(8),
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(8),
      I1 => sel0(1),
      I2 => timer_reg(8),
      I3 => sel0(0),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => sel0(2),
      I2 => crcOut(9),
      I3 => sel0(1),
      I4 => crcIn(9),
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8830"
    )
        port map (
      I0 => localTimer(9),
      I1 => sel0(1),
      I2 => timer_reg(9),
      I3 => sel0(0),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
busyIn_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AEEEEEEE"
    )
        port map (
      I0 => crcIn19_out,
      I1 => busyIn,
      I2 => ctrIn(0),
      I3 => ctrIn(1),
      I4 => ctrIn(2),
      I5 => \crcIn[15]_i_1_n_0\,
      O => busyIn_i_1_n_0
    );
busyIn_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => busyIn_i_3_n_0,
      I1 => p_1_in(1),
      I2 => busyIn,
      O => crcIn19_out
    );
busyIn_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00800000"
    )
        port map (
      I0 => s00_axi_wstrb(1),
      I1 => s00_axi_wstrb(0),
      I2 => p_1_in(0),
      I3 => p_1_in(2),
      I4 => \^s_axi_wready\,
      O => busyIn_i_3_n_0
    );
busyIn_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => busyIn_i_1_n_0,
      Q => busyIn,
      R => '0'
    );
busyOut_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AEEEEEEE"
    )
        port map (
      I0 => crcOut15_out,
      I1 => busyOut,
      I2 => ctrOut(0),
      I3 => ctrOut(1),
      I4 => ctrOut(2),
      I5 => \crcOut[15]_i_1_n_0\,
      O => busyOut_i_1_n_0
    );
busyOut_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => busyIn_i_3_n_0,
      I1 => p_1_in(1),
      I2 => busyOut,
      O => crcOut15_out
    );
busyOut_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => busyOut_i_1_n_0,
      Q => busyOut,
      R => '0'
    );
\crcIn[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"56A6"
    )
        port map (
      I0 => crcIn(0),
      I1 => crcIn(1),
      I2 => crcIn19_out,
      I3 => s00_axi_wdata(0),
      O => \crcIn[0]_i_1_n_0\
    );
\crcIn[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(10),
      I1 => crcIn19_out,
      I2 => crcIn(11),
      O => \crcIn[10]_i_1_n_0\
    );
\crcIn[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(11),
      I1 => crcIn19_out,
      I2 => crcIn(12),
      O => \crcIn[11]_i_1_n_0\
    );
\crcIn[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(12),
      I1 => crcIn19_out,
      I2 => crcIn(13),
      O => \crcIn[12]_i_1_n_0\
    );
\crcIn[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => crcIn(13),
      I1 => crcIn19_out,
      I2 => crcIn(0),
      I3 => crcIn(14),
      O => \crcIn[13]_i_1_n_0\
    );
\crcIn[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(14),
      I1 => crcIn19_out,
      I2 => crcIn(15),
      O => \crcIn[14]_i_1_n_0\
    );
\crcIn[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => p_1_in(0),
      I1 => p_1_in(2),
      I2 => \crcIn[15]_i_4_n_0\,
      O => \crcIn[15]_i_1_n_0\
    );
\crcIn[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => busyIn,
      I1 => crcIn19_out,
      O => \crcIn[15]_i_2_n_0\
    );
\crcIn[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(15),
      I1 => crcIn19_out,
      I2 => crcIn(0),
      O => \crcIn[15]_i_3_n_0\
    );
\crcIn[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFE0000"
    )
        port map (
      I0 => s00_axi_wstrb(3),
      I1 => s00_axi_wstrb(1),
      I2 => s00_axi_wstrb(0),
      I3 => s00_axi_wstrb(2),
      I4 => \^s_axi_wready\,
      I5 => p_1_in(1),
      O => \crcIn[15]_i_4_n_0\
    );
\crcIn[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(1),
      I1 => crcIn(1),
      I2 => crcIn19_out,
      I3 => crcIn(2),
      O => \crcIn[1]_i_1_n_0\
    );
\crcIn[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(2),
      I1 => crcIn(2),
      I2 => crcIn19_out,
      I3 => crcIn(3),
      O => \crcIn[2]_i_1_n_0\
    );
\crcIn[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(3),
      I1 => crcIn(3),
      I2 => crcIn19_out,
      I3 => crcIn(4),
      O => \crcIn[3]_i_1_n_0\
    );
\crcIn[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(4),
      I1 => crcIn(4),
      I2 => crcIn19_out,
      I3 => crcIn(5),
      O => \crcIn[4]_i_1_n_0\
    );
\crcIn[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(5),
      I1 => crcIn(5),
      I2 => crcIn19_out,
      I3 => crcIn(6),
      O => \crcIn[5]_i_1_n_0\
    );
\crcIn[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(6),
      I1 => crcIn(6),
      I2 => crcIn19_out,
      I3 => crcIn(7),
      O => \crcIn[6]_i_1_n_0\
    );
\crcIn[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(7),
      I1 => crcIn(7),
      I2 => crcIn19_out,
      I3 => crcIn(8),
      O => \crcIn[7]_i_1_n_0\
    );
\crcIn[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(8),
      I1 => crcIn19_out,
      I2 => crcIn(9),
      O => \crcIn[8]_i_1_n_0\
    );
\crcIn[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcIn(9),
      I1 => crcIn19_out,
      I2 => crcIn(10),
      O => \crcIn[9]_i_1_n_0\
    );
\crcIn_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[0]_i_1_n_0\,
      Q => crcIn(0),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[10]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[10]_i_1_n_0\,
      Q => crcIn(10),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[11]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[11]_i_1_n_0\,
      Q => crcIn(11),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[12]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[12]_i_1_n_0\,
      Q => crcIn(12),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[13]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[13]_i_1_n_0\,
      Q => crcIn(13),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[14]_i_1_n_0\,
      Q => crcIn(14),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[15]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[15]_i_3_n_0\,
      Q => crcIn(15),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[1]_i_1_n_0\,
      Q => crcIn(1),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[2]_i_1_n_0\,
      Q => crcIn(2),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[3]_i_1_n_0\,
      Q => crcIn(3),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[4]_i_1_n_0\,
      Q => crcIn(4),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[5]_i_1_n_0\,
      Q => crcIn(5),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[6]_i_1_n_0\,
      Q => crcIn(6),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[7]_i_1_n_0\,
      Q => crcIn(7),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[8]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[8]_i_1_n_0\,
      Q => crcIn(8),
      S => \crcIn[15]_i_1_n_0\
    );
\crcIn_reg[9]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcIn[15]_i_2_n_0\,
      D => \crcIn[9]_i_1_n_0\,
      Q => crcIn(9),
      S => \crcIn[15]_i_1_n_0\
    );
\crcOut[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"56A6"
    )
        port map (
      I0 => crcOut(0),
      I1 => crcOut(1),
      I2 => crcOut15_out,
      I3 => s00_axi_wdata(0),
      O => \crcOut[0]_i_1_n_0\
    );
\crcOut[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(10),
      I1 => crcOut15_out,
      I2 => crcOut(11),
      O => \crcOut[10]_i_1_n_0\
    );
\crcOut[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(11),
      I1 => crcOut15_out,
      I2 => crcOut(12),
      O => \crcOut[11]_i_1_n_0\
    );
\crcOut[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(12),
      I1 => crcOut15_out,
      I2 => crcOut(13),
      O => \crcOut[12]_i_1_n_0\
    );
\crcOut[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8BB8"
    )
        port map (
      I0 => crcOut(13),
      I1 => crcOut15_out,
      I2 => crcOut(0),
      I3 => crcOut(14),
      O => \crcOut[13]_i_1_n_0\
    );
\crcOut[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(14),
      I1 => crcOut15_out,
      I2 => crcOut(15),
      O => \crcOut[14]_i_1_n_0\
    );
\crcOut[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => p_1_in(0),
      I1 => p_1_in(2),
      I2 => \crcOut[15]_i_4_n_0\,
      O => \crcOut[15]_i_1_n_0\
    );
\crcOut[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => busyOut,
      I1 => crcOut15_out,
      O => \crcOut[15]_i_2_n_0\
    );
\crcOut[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(15),
      I1 => crcOut15_out,
      I2 => crcOut(0),
      O => \crcOut[15]_i_3_n_0\
    );
\crcOut[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFFFFFFFFFF"
    )
        port map (
      I0 => s00_axi_wstrb(3),
      I1 => s00_axi_wstrb(1),
      I2 => s00_axi_wstrb(0),
      I3 => s00_axi_wstrb(2),
      I4 => p_1_in(1),
      I5 => \^s_axi_wready\,
      O => \crcOut[15]_i_4_n_0\
    );
\crcOut[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(1),
      I1 => crcOut(1),
      I2 => crcOut15_out,
      I3 => crcOut(2),
      O => \crcOut[1]_i_1_n_0\
    );
\crcOut[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(2),
      I1 => crcOut(2),
      I2 => crcOut15_out,
      I3 => crcOut(3),
      O => \crcOut[2]_i_1_n_0\
    );
\crcOut[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(3),
      I1 => crcOut(3),
      I2 => crcOut15_out,
      I3 => crcOut(4),
      O => \crcOut[3]_i_1_n_0\
    );
\crcOut[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(4),
      I1 => crcOut(4),
      I2 => crcOut15_out,
      I3 => crcOut(5),
      O => \crcOut[4]_i_1_n_0\
    );
\crcOut[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(5),
      I1 => crcOut(5),
      I2 => crcOut15_out,
      I3 => crcOut(6),
      O => \crcOut[5]_i_1_n_0\
    );
\crcOut[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(6),
      I1 => crcOut(6),
      I2 => crcOut15_out,
      I3 => crcOut(7),
      O => \crcOut[6]_i_1_n_0\
    );
\crcOut[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => s00_axi_wdata(7),
      I1 => crcOut(7),
      I2 => crcOut15_out,
      I3 => crcOut(8),
      O => \crcOut[7]_i_1_n_0\
    );
\crcOut[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(8),
      I1 => crcOut15_out,
      I2 => crcOut(9),
      O => \crcOut[8]_i_1_n_0\
    );
\crcOut[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => crcOut(9),
      I1 => crcOut15_out,
      I2 => crcOut(10),
      O => \crcOut[9]_i_1_n_0\
    );
\crcOut_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[0]_i_1_n_0\,
      Q => crcOut(0),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[10]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[10]_i_1_n_0\,
      Q => crcOut(10),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[11]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[11]_i_1_n_0\,
      Q => crcOut(11),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[12]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[12]_i_1_n_0\,
      Q => crcOut(12),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[13]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[13]_i_1_n_0\,
      Q => crcOut(13),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[14]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[14]_i_1_n_0\,
      Q => crcOut(14),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[15]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[15]_i_3_n_0\,
      Q => crcOut(15),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[1]_i_1_n_0\,
      Q => crcOut(1),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[2]_i_1_n_0\,
      Q => crcOut(2),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[3]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[3]_i_1_n_0\,
      Q => crcOut(3),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[4]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[4]_i_1_n_0\,
      Q => crcOut(4),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[5]_i_1_n_0\,
      Q => crcOut(5),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[6]_i_1_n_0\,
      Q => crcOut(6),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[7]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[7]_i_1_n_0\,
      Q => crcOut(7),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[8]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[8]_i_1_n_0\,
      Q => crcOut(8),
      S => \crcOut[15]_i_1_n_0\
    );
\crcOut_reg[9]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \crcOut[15]_i_2_n_0\,
      D => \crcOut[9]_i_1_n_0\,
      Q => crcOut(9),
      S => \crcOut[15]_i_1_n_0\
    );
\ctrIn[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0006"
    )
        port map (
      I0 => ctrIn(0),
      I1 => busyIn,
      I2 => \crcIn[15]_i_1_n_0\,
      I3 => crcIn19_out,
      O => \ctrIn[0]_i_1_n_0\
    );
\ctrIn[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000006A"
    )
        port map (
      I0 => ctrIn(1),
      I1 => busyIn,
      I2 => ctrIn(0),
      I3 => \crcIn[15]_i_1_n_0\,
      I4 => crcIn19_out,
      O => \ctrIn[1]_i_1_n_0\
    );
\ctrIn[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000006AAA"
    )
        port map (
      I0 => ctrIn(2),
      I1 => busyIn,
      I2 => ctrIn(0),
      I3 => ctrIn(1),
      I4 => \crcIn[15]_i_1_n_0\,
      I5 => crcIn19_out,
      O => \ctrIn[2]_i_1_n_0\
    );
\ctrIn_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrIn[0]_i_1_n_0\,
      Q => ctrIn(0),
      R => '0'
    );
\ctrIn_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrIn[1]_i_1_n_0\,
      Q => ctrIn(1),
      R => '0'
    );
\ctrIn_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrIn[2]_i_1_n_0\,
      Q => ctrIn(2),
      R => '0'
    );
\ctrOut[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0006"
    )
        port map (
      I0 => ctrOut(0),
      I1 => busyOut,
      I2 => \crcOut[15]_i_1_n_0\,
      I3 => crcOut15_out,
      O => \ctrOut[0]_i_1_n_0\
    );
\ctrOut[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000006A"
    )
        port map (
      I0 => ctrOut(1),
      I1 => busyOut,
      I2 => ctrOut(0),
      I3 => \crcOut[15]_i_1_n_0\,
      I4 => crcOut15_out,
      O => \ctrOut[1]_i_1_n_0\
    );
\ctrOut[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000006AAA"
    )
        port map (
      I0 => ctrOut(2),
      I1 => busyOut,
      I2 => ctrOut(0),
      I3 => ctrOut(1),
      I4 => \crcOut[15]_i_1_n_0\,
      I5 => crcOut15_out,
      O => \ctrOut[2]_i_1_n_0\
    );
\ctrOut_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrOut[0]_i_1_n_0\,
      Q => ctrOut(0),
      R => '0'
    );
\ctrOut_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrOut[1]_i_1_n_0\,
      Q => ctrOut(1),
      R => '0'
    );
\ctrOut_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ctrOut[2]_i_1_n_0\,
      Q => ctrOut(2),
      R => '0'
    );
localTimer0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => localTimer0_carry_n_0,
      CO(2) => localTimer0_carry_n_1,
      CO(1) => localTimer0_carry_n_2,
      CO(0) => localTimer0_carry_n_3,
      CYINIT => localTimer(0),
      DI(3 downto 0) => localTimer(4 downto 1),
      O(3 downto 0) => localTimer0(4 downto 1),
      S(3) => localTimer0_carry_i_1_n_0,
      S(2) => localTimer0_carry_i_2_n_0,
      S(1) => localTimer0_carry_i_3_n_0,
      S(0) => localTimer0_carry_i_4_n_0
    );
\localTimer0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => localTimer0_carry_n_0,
      CO(3) => \localTimer0_carry__0_n_0\,
      CO(2) => \localTimer0_carry__0_n_1\,
      CO(1) => \localTimer0_carry__0_n_2\,
      CO(0) => \localTimer0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(8 downto 5),
      O(3 downto 0) => localTimer0(8 downto 5),
      S(3) => \localTimer0_carry__0_i_1_n_0\,
      S(2) => \localTimer0_carry__0_i_2_n_0\,
      S(1) => \localTimer0_carry__0_i_3_n_0\,
      S(0) => \localTimer0_carry__0_i_4_n_0\
    );
\localTimer0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(8),
      O => \localTimer0_carry__0_i_1_n_0\
    );
\localTimer0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(7),
      O => \localTimer0_carry__0_i_2_n_0\
    );
\localTimer0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(6),
      O => \localTimer0_carry__0_i_3_n_0\
    );
\localTimer0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(5),
      O => \localTimer0_carry__0_i_4_n_0\
    );
\localTimer0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__0_n_0\,
      CO(3) => \localTimer0_carry__1_n_0\,
      CO(2) => \localTimer0_carry__1_n_1\,
      CO(1) => \localTimer0_carry__1_n_2\,
      CO(0) => \localTimer0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(12 downto 9),
      O(3 downto 0) => localTimer0(12 downto 9),
      S(3) => \localTimer0_carry__1_i_1_n_0\,
      S(2) => \localTimer0_carry__1_i_2_n_0\,
      S(1) => \localTimer0_carry__1_i_3_n_0\,
      S(0) => \localTimer0_carry__1_i_4_n_0\
    );
\localTimer0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(12),
      O => \localTimer0_carry__1_i_1_n_0\
    );
\localTimer0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(11),
      O => \localTimer0_carry__1_i_2_n_0\
    );
\localTimer0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(10),
      O => \localTimer0_carry__1_i_3_n_0\
    );
\localTimer0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(9),
      O => \localTimer0_carry__1_i_4_n_0\
    );
\localTimer0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__1_n_0\,
      CO(3) => \localTimer0_carry__2_n_0\,
      CO(2) => \localTimer0_carry__2_n_1\,
      CO(1) => \localTimer0_carry__2_n_2\,
      CO(0) => \localTimer0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(16 downto 13),
      O(3 downto 0) => localTimer0(16 downto 13),
      S(3) => \localTimer0_carry__2_i_1_n_0\,
      S(2) => \localTimer0_carry__2_i_2_n_0\,
      S(1) => \localTimer0_carry__2_i_3_n_0\,
      S(0) => \localTimer0_carry__2_i_4_n_0\
    );
\localTimer0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(16),
      O => \localTimer0_carry__2_i_1_n_0\
    );
\localTimer0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(15),
      O => \localTimer0_carry__2_i_2_n_0\
    );
\localTimer0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(14),
      O => \localTimer0_carry__2_i_3_n_0\
    );
\localTimer0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(13),
      O => \localTimer0_carry__2_i_4_n_0\
    );
\localTimer0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__2_n_0\,
      CO(3) => \localTimer0_carry__3_n_0\,
      CO(2) => \localTimer0_carry__3_n_1\,
      CO(1) => \localTimer0_carry__3_n_2\,
      CO(0) => \localTimer0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(20 downto 17),
      O(3 downto 0) => localTimer0(20 downto 17),
      S(3) => \localTimer0_carry__3_i_1_n_0\,
      S(2) => \localTimer0_carry__3_i_2_n_0\,
      S(1) => \localTimer0_carry__3_i_3_n_0\,
      S(0) => \localTimer0_carry__3_i_4_n_0\
    );
\localTimer0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(20),
      O => \localTimer0_carry__3_i_1_n_0\
    );
\localTimer0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(19),
      O => \localTimer0_carry__3_i_2_n_0\
    );
\localTimer0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(18),
      O => \localTimer0_carry__3_i_3_n_0\
    );
\localTimer0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(17),
      O => \localTimer0_carry__3_i_4_n_0\
    );
\localTimer0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__3_n_0\,
      CO(3) => \localTimer0_carry__4_n_0\,
      CO(2) => \localTimer0_carry__4_n_1\,
      CO(1) => \localTimer0_carry__4_n_2\,
      CO(0) => \localTimer0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(24 downto 21),
      O(3 downto 0) => localTimer0(24 downto 21),
      S(3) => \localTimer0_carry__4_i_1_n_0\,
      S(2) => \localTimer0_carry__4_i_2_n_0\,
      S(1) => \localTimer0_carry__4_i_3_n_0\,
      S(0) => \localTimer0_carry__4_i_4_n_0\
    );
\localTimer0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(24),
      O => \localTimer0_carry__4_i_1_n_0\
    );
\localTimer0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(23),
      O => \localTimer0_carry__4_i_2_n_0\
    );
\localTimer0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(22),
      O => \localTimer0_carry__4_i_3_n_0\
    );
\localTimer0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(21),
      O => \localTimer0_carry__4_i_4_n_0\
    );
\localTimer0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__4_n_0\,
      CO(3) => \localTimer0_carry__5_n_0\,
      CO(2) => \localTimer0_carry__5_n_1\,
      CO(1) => \localTimer0_carry__5_n_2\,
      CO(0) => \localTimer0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => localTimer(28 downto 25),
      O(3 downto 0) => localTimer0(28 downto 25),
      S(3) => \localTimer0_carry__5_i_1_n_0\,
      S(2) => \localTimer0_carry__5_i_2_n_0\,
      S(1) => \localTimer0_carry__5_i_3_n_0\,
      S(0) => \localTimer0_carry__5_i_4_n_0\
    );
\localTimer0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(28),
      O => \localTimer0_carry__5_i_1_n_0\
    );
\localTimer0_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(27),
      O => \localTimer0_carry__5_i_2_n_0\
    );
\localTimer0_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(26),
      O => \localTimer0_carry__5_i_3_n_0\
    );
\localTimer0_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(25),
      O => \localTimer0_carry__5_i_4_n_0\
    );
\localTimer0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \localTimer0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_localTimer0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \localTimer0_carry__6_n_2\,
      CO(0) => \localTimer0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => localTimer(30 downto 29),
      O(3) => \NLW_localTimer0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => localTimer0(31 downto 29),
      S(3) => '0',
      S(2) => \localTimer0_carry__6_i_1_n_0\,
      S(1) => \localTimer0_carry__6_i_2_n_0\,
      S(0) => \localTimer0_carry__6_i_3_n_0\
    );
\localTimer0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(31),
      O => \localTimer0_carry__6_i_1_n_0\
    );
\localTimer0_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(30),
      O => \localTimer0_carry__6_i_2_n_0\
    );
\localTimer0_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(29),
      O => \localTimer0_carry__6_i_3_n_0\
    );
localTimer0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(4),
      O => localTimer0_carry_i_1_n_0
    );
localTimer0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(3),
      O => localTimer0_carry_i_2_n_0
    );
localTimer0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(2),
      O => localTimer0_carry_i_3_n_0
    );
localTimer0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => localTimer(1),
      O => localTimer0_carry_i_4_n_0
    );
\localTimer[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8ABA"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => \localTimer[31]_i_7_n_0\,
      I3 => localTimer(0),
      O => \localTimer[0]_i_1_n_0\
    );
\localTimer[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(10),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(10),
      O => \localTimer[10]_i_1_n_0\
    );
\localTimer[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(11),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(11),
      O => \localTimer[11]_i_1_n_0\
    );
\localTimer[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(12),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(12),
      O => \localTimer[12]_i_1_n_0\
    );
\localTimer[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(13),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(13),
      O => \localTimer[13]_i_1_n_0\
    );
\localTimer[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(14),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(14),
      O => \localTimer[14]_i_1_n_0\
    );
\localTimer[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFDD0D0000"
    )
        port map (
      I0 => \localTimer[31]_i_3_n_0\,
      I1 => \localTimer[31]_i_4_n_0\,
      I2 => \localTimer[31]_i_5_n_0\,
      I3 => \localTimer[31]_i_6_n_0\,
      I4 => \localTimer[31]_i_7_n_0\,
      I5 => \localTimer[15]_i_3_n_0\,
      O => \localTimer[15]_i_1_n_0\
    );
\localTimer[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(15),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(15),
      O => \localTimer[15]_i_2_n_0\
    );
\localTimer[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => \localTimer[31]_i_9_n_0\,
      I1 => \crcOut[15]_i_4_n_0\,
      I2 => p_1_in(0),
      I3 => p_1_in(2),
      I4 => s00_axi_wstrb(1),
      O => \localTimer[15]_i_3_n_0\
    );
\localTimer[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(16),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(16),
      O => \localTimer[16]_i_1_n_0\
    );
\localTimer[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(17),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(17),
      O => \localTimer[17]_i_1_n_0\
    );
\localTimer[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(18),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(18),
      O => \localTimer[18]_i_1_n_0\
    );
\localTimer[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(19),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(19),
      O => \localTimer[19]_i_1_n_0\
    );
\localTimer[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(1),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(1),
      O => \localTimer[1]_i_1_n_0\
    );
\localTimer[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(20),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(20),
      O => \localTimer[20]_i_1_n_0\
    );
\localTimer[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(21),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(21),
      O => \localTimer[21]_i_1_n_0\
    );
\localTimer[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(22),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(22),
      O => \localTimer[22]_i_1_n_0\
    );
\localTimer[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFDD0D0000"
    )
        port map (
      I0 => \localTimer[31]_i_3_n_0\,
      I1 => \localTimer[31]_i_4_n_0\,
      I2 => \localTimer[31]_i_5_n_0\,
      I3 => \localTimer[31]_i_6_n_0\,
      I4 => \localTimer[31]_i_7_n_0\,
      I5 => \localTimer[23]_i_3_n_0\,
      O => \localTimer[23]_i_1_n_0\
    );
\localTimer[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(23),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(23),
      O => \localTimer[23]_i_2_n_0\
    );
\localTimer[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => \localTimer[31]_i_9_n_0\,
      I1 => \crcOut[15]_i_4_n_0\,
      I2 => p_1_in(0),
      I3 => p_1_in(2),
      I4 => s00_axi_wstrb(2),
      O => \localTimer[23]_i_3_n_0\
    );
\localTimer[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(24),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(24),
      O => \localTimer[24]_i_1_n_0\
    );
\localTimer[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(25),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(25),
      O => \localTimer[25]_i_1_n_0\
    );
\localTimer[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(26),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(26),
      O => \localTimer[26]_i_1_n_0\
    );
\localTimer[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(27),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(27),
      O => \localTimer[27]_i_1_n_0\
    );
\localTimer[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(28),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(28),
      O => \localTimer[28]_i_1_n_0\
    );
\localTimer[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(29),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(29),
      O => \localTimer[29]_i_1_n_0\
    );
\localTimer[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(2),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(2),
      O => \localTimer[2]_i_1_n_0\
    );
\localTimer[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(30),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(30),
      O => \localTimer[30]_i_1_n_0\
    );
\localTimer[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFDD0D0000"
    )
        port map (
      I0 => \localTimer[31]_i_3_n_0\,
      I1 => \localTimer[31]_i_4_n_0\,
      I2 => \localTimer[31]_i_5_n_0\,
      I3 => \localTimer[31]_i_6_n_0\,
      I4 => \localTimer[31]_i_7_n_0\,
      I5 => \localTimer[31]_i_8_n_0\,
      O => \localTimer[31]_i_1_n_0\
    );
\localTimer[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(2),
      I1 => localTimer(3),
      I2 => localTimer(0),
      I3 => localTimer(1),
      O => \localTimer[31]_i_10_n_0\
    );
\localTimer[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => localTimer(4),
      I1 => localTimer(5),
      I2 => localTimer(6),
      I3 => localTimer(7),
      O => \localTimer[31]_i_11_n_0\
    );
\localTimer[31]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(12),
      I1 => localTimer(13),
      I2 => localTimer(14),
      I3 => localTimer(15),
      O => \localTimer[31]_i_12_n_0\
    );
\localTimer[31]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(9),
      I1 => localTimer(10),
      I2 => localTimer(8),
      I3 => localTimer(11),
      O => \localTimer[31]_i_13_n_0\
    );
\localTimer[31]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(29),
      I1 => localTimer(30),
      I2 => localTimer(28),
      I3 => localTimer(31),
      O => \localTimer[31]_i_14_n_0\
    );
\localTimer[31]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(25),
      I1 => localTimer(26),
      I2 => localTimer(24),
      I3 => localTimer(27),
      O => \localTimer[31]_i_15_n_0\
    );
\localTimer[31]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(16),
      I1 => localTimer(17),
      I2 => localTimer(18),
      I3 => localTimer(19),
      O => \localTimer[31]_i_16_n_0\
    );
\localTimer[31]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => localTimer(20),
      I1 => localTimer(23),
      I2 => localTimer(21),
      I3 => localTimer(22),
      O => \localTimer[31]_i_17_n_0\
    );
\localTimer[31]_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(0),
      I1 => localTimer(1),
      I2 => localTimer(2),
      I3 => localTimer(3),
      O => \localTimer[31]_i_18_n_0\
    );
\localTimer[31]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => localTimer(6),
      I1 => localTimer(7),
      I2 => localTimer(4),
      I3 => localTimer(5),
      O => \localTimer[31]_i_19_n_0\
    );
\localTimer[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(31),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(31),
      O => \localTimer[31]_i_2_n_0\
    );
\localTimer[31]_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(13),
      I1 => localTimer(14),
      I2 => localTimer(12),
      I3 => localTimer(15),
      O => \localTimer[31]_i_20_n_0\
    );
\localTimer[31]_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(8),
      I1 => localTimer(11),
      I2 => localTimer(9),
      I3 => localTimer(10),
      O => \localTimer[31]_i_21_n_0\
    );
\localTimer[31]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(18),
      I1 => localTimer(19),
      I2 => localTimer(16),
      I3 => localTimer(17),
      O => \localTimer[31]_i_22_n_0\
    );
\localTimer[31]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(20),
      I1 => localTimer(23),
      I2 => localTimer(21),
      I3 => localTimer(22),
      O => \localTimer[31]_i_23_n_0\
    );
\localTimer[31]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(30),
      I1 => localTimer(31),
      I2 => localTimer(28),
      I3 => localTimer(29),
      O => \localTimer[31]_i_24_n_0\
    );
\localTimer[31]_i_25\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => localTimer(25),
      I1 => localTimer(26),
      I2 => localTimer(24),
      I3 => localTimer(27),
      O => \localTimer[31]_i_25_n_0\
    );
\localTimer[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \localTimer[31]_i_10_n_0\,
      I1 => \localTimer[31]_i_11_n_0\,
      I2 => \localTimer[31]_i_12_n_0\,
      I3 => \localTimer[31]_i_13_n_0\,
      O => \localTimer[31]_i_3_n_0\
    );
\localTimer[31]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \localTimer[31]_i_14_n_0\,
      I1 => \localTimer[31]_i_15_n_0\,
      I2 => \localTimer[31]_i_16_n_0\,
      I3 => \localTimer[31]_i_17_n_0\,
      O => \localTimer[31]_i_4_n_0\
    );
\localTimer[31]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \localTimer[31]_i_18_n_0\,
      I1 => \localTimer[31]_i_19_n_0\,
      I2 => \localTimer[31]_i_20_n_0\,
      I3 => \localTimer[31]_i_21_n_0\,
      O => \localTimer[31]_i_5_n_0\
    );
\localTimer[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \localTimer[31]_i_22_n_0\,
      I1 => \localTimer[31]_i_23_n_0\,
      I2 => \localTimer[31]_i_24_n_0\,
      I3 => \localTimer[31]_i_25_n_0\,
      O => \localTimer[31]_i_6_n_0\
    );
\localTimer[31]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \crcOut[15]_i_4_n_0\,
      I1 => p_1_in(0),
      I2 => p_1_in(2),
      O => \localTimer[31]_i_7_n_0\
    );
\localTimer[31]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => \localTimer[31]_i_9_n_0\,
      I1 => \crcOut[15]_i_4_n_0\,
      I2 => p_1_in(0),
      I3 => p_1_in(2),
      I4 => s00_axi_wstrb(3),
      O => \localTimer[31]_i_8_n_0\
    );
\localTimer[31]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40000000"
    )
        port map (
      I0 => p_1_in(0),
      I1 => p_1_in(2),
      I2 => p_1_in(1),
      I3 => \^s_axi_wready\,
      I4 => s00_axi_wstrb(0),
      O => \localTimer[31]_i_9_n_0\
    );
\localTimer[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(3),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(3),
      O => \localTimer[3]_i_1_n_0\
    );
\localTimer[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(4),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(4),
      O => \localTimer[4]_i_1_n_0\
    );
\localTimer[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(5),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(5),
      O => \localTimer[5]_i_1_n_0\
    );
\localTimer[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(6),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(6),
      O => \localTimer[6]_i_1_n_0\
    );
\localTimer[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD0D0000FFFFFFFF"
    )
        port map (
      I0 => \localTimer[31]_i_3_n_0\,
      I1 => \localTimer[31]_i_4_n_0\,
      I2 => \localTimer[31]_i_5_n_0\,
      I3 => \localTimer[31]_i_6_n_0\,
      I4 => \localTimer[31]_i_7_n_0\,
      I5 => \localTimer[7]_i_3_n_0\,
      O => \localTimer[7]_i_1_n_0\
    );
\localTimer[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(7),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(7),
      O => \localTimer[7]_i_2_n_0\
    );
\localTimer[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => s00_axi_wstrb(0),
      I1 => \^s_axi_wready\,
      I2 => p_1_in(1),
      I3 => p_1_in(2),
      O => \localTimer[7]_i_3_n_0\
    );
\localTimer[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(8),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(8),
      O => \localTimer[8]_i_1_n_0\
    );
\localTimer[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => localTimer0(9),
      I3 => \localTimer[31]_i_7_n_0\,
      I4 => s00_axi_wdata(9),
      O => \localTimer[9]_i_1_n_0\
    );
\localTimer_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[0]_i_1_n_0\,
      Q => localTimer(0),
      R => '0'
    );
\localTimer_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[10]_i_1_n_0\,
      Q => localTimer(10),
      R => '0'
    );
\localTimer_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[11]_i_1_n_0\,
      Q => localTimer(11),
      R => '0'
    );
\localTimer_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[12]_i_1_n_0\,
      Q => localTimer(12),
      R => '0'
    );
\localTimer_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[13]_i_1_n_0\,
      Q => localTimer(13),
      R => '0'
    );
\localTimer_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[14]_i_1_n_0\,
      Q => localTimer(14),
      R => '0'
    );
\localTimer_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[15]_i_2_n_0\,
      Q => localTimer(15),
      R => '0'
    );
\localTimer_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[16]_i_1_n_0\,
      Q => localTimer(16),
      R => '0'
    );
\localTimer_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[17]_i_1_n_0\,
      Q => localTimer(17),
      R => '0'
    );
\localTimer_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[18]_i_1_n_0\,
      Q => localTimer(18),
      R => '0'
    );
\localTimer_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[19]_i_1_n_0\,
      Q => localTimer(19),
      R => '0'
    );
\localTimer_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[1]_i_1_n_0\,
      Q => localTimer(1),
      R => '0'
    );
\localTimer_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[20]_i_1_n_0\,
      Q => localTimer(20),
      R => '0'
    );
\localTimer_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[21]_i_1_n_0\,
      Q => localTimer(21),
      R => '0'
    );
\localTimer_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[22]_i_1_n_0\,
      Q => localTimer(22),
      R => '0'
    );
\localTimer_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[23]_i_1_n_0\,
      D => \localTimer[23]_i_2_n_0\,
      Q => localTimer(23),
      R => '0'
    );
\localTimer_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[24]_i_1_n_0\,
      Q => localTimer(24),
      R => '0'
    );
\localTimer_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[25]_i_1_n_0\,
      Q => localTimer(25),
      R => '0'
    );
\localTimer_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[26]_i_1_n_0\,
      Q => localTimer(26),
      R => '0'
    );
\localTimer_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[27]_i_1_n_0\,
      Q => localTimer(27),
      R => '0'
    );
\localTimer_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[28]_i_1_n_0\,
      Q => localTimer(28),
      R => '0'
    );
\localTimer_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[29]_i_1_n_0\,
      Q => localTimer(29),
      R => '0'
    );
\localTimer_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[2]_i_1_n_0\,
      Q => localTimer(2),
      R => '0'
    );
\localTimer_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[30]_i_1_n_0\,
      Q => localTimer(30),
      R => '0'
    );
\localTimer_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[31]_i_1_n_0\,
      D => \localTimer[31]_i_2_n_0\,
      Q => localTimer(31),
      R => '0'
    );
\localTimer_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[3]_i_1_n_0\,
      Q => localTimer(3),
      R => '0'
    );
\localTimer_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[4]_i_1_n_0\,
      Q => localTimer(4),
      R => '0'
    );
\localTimer_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[5]_i_1_n_0\,
      Q => localTimer(5),
      R => '0'
    );
\localTimer_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[6]_i_1_n_0\,
      Q => localTimer(6),
      R => '0'
    );
\localTimer_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[7]_i_1_n_0\,
      D => \localTimer[7]_i_2_n_0\,
      Q => localTimer(7),
      R => '0'
    );
\localTimer_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[8]_i_1_n_0\,
      Q => localTimer(8),
      R => '0'
    );
\localTimer_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \localTimer[15]_i_1_n_0\,
      D => \localTimer[9]_i_1_n_0\,
      Q => localTimer(9),
      R => '0'
    );
local_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBB8BBBBBBBB"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \localTimer[31]_i_9_n_0\,
      I2 => local_i_2_n_0,
      I3 => local_i_3_n_0,
      I4 => local_i_4_n_0,
      I5 => local_i_5_n_0,
      O => local_i_1_n_0
    );
local_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => localTimer(22),
      I1 => localTimer(21),
      I2 => localTimer(23),
      I3 => localTimer(20),
      I4 => \localTimer[31]_i_16_n_0\,
      O => local_i_2_n_0
    );
local_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => localTimer(27),
      I1 => localTimer(24),
      I2 => localTimer(26),
      I3 => localTimer(25),
      I4 => \localTimer[31]_i_14_n_0\,
      O => local_i_3_n_0
    );
local_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => localTimer(11),
      I1 => localTimer(8),
      I2 => localTimer(10),
      I3 => localTimer(9),
      I4 => \localTimer[31]_i_12_n_0\,
      O => local_i_4_n_0
    );
local_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => localTimer(7),
      I1 => localTimer(6),
      I2 => localTimer(5),
      I3 => localTimer(4),
      I4 => \localTimer[31]_i_10_n_0\,
      O => local_i_5_n_0
    );
local_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => local_i_1_n_0,
      Q => local,
      R => '0'
    );
\slaveAddr[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFFE"
    )
        port map (
      I0 => \slaveAddr[7]_i_2_n_0\,
      I1 => s00_axi_wdata(5),
      I2 => s00_axi_wdata(2),
      I3 => s00_axi_wdata(4),
      I4 => s00_axi_wdata(1),
      I5 => \slaveAddr[7]_i_3_n_0\,
      O => slaveAddr0
    );
\slaveAddr[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => s00_axi_wdata(3),
      I1 => s00_axi_wdata(0),
      I2 => s00_axi_wdata(7),
      I3 => s00_axi_wdata(6),
      O => \slaveAddr[7]_i_2_n_0\
    );
\slaveAddr[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \slaveAddr[7]_i_4_n_0\,
      I1 => p_1_in(2),
      I2 => p_1_in(0),
      I3 => p_1_in(1),
      I4 => \^s_axi_wready\,
      O => \slaveAddr[7]_i_3_n_0\
    );
\slaveAddr[7]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => s00_axi_wdata(7),
      I1 => s00_axi_wdata(4),
      I2 => s00_axi_wdata(3),
      I3 => s00_axi_wdata(5),
      I4 => s00_axi_wdata(6),
      O => \slaveAddr[7]_i_4_n_0\
    );
\slaveAddr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(0),
      Q => slaveAddr(0),
      R => '0'
    );
\slaveAddr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(1),
      Q => slaveAddr(1),
      R => '0'
    );
\slaveAddr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(2),
      Q => slaveAddr(2),
      R => '0'
    );
\slaveAddr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(3),
      Q => slaveAddr(3),
      R => '0'
    );
\slaveAddr_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(4),
      Q => slaveAddr(4),
      R => '0'
    );
\slaveAddr_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(5),
      Q => slaveAddr(5),
      R => '0'
    );
\slaveAddr_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(6),
      Q => slaveAddr(6),
      R => '0'
    );
\slaveAddr_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => slaveAddr0,
      D => s00_axi_wdata(7),
      Q => slaveAddr(7),
      R => '0'
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^s00_axi_rvalid\,
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      O => \slv_reg_rden__0\
    );
\timer[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \crcIn[15]_i_4_n_0\,
      I1 => p_1_in(2),
      I2 => p_1_in(0),
      O => timer0
    );
\timer[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => timer_reg(29),
      I1 => timer_reg(30),
      I2 => timer_reg(24),
      I3 => timer_reg(25),
      O => \timer[0]_i_10_n_0\
    );
\timer[0]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => timer_reg(11),
      I1 => timer_reg(7),
      I2 => timer_reg(12),
      I3 => timer_reg(15),
      O => \timer[0]_i_11_n_0\
    );
\timer[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => timer_reg(10),
      I1 => timer_reg(0),
      I2 => timer_reg(17),
      I3 => timer_reg(22),
      O => \timer[0]_i_12_n_0\
    );
\timer[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \timer[0]_i_4_n_0\,
      I1 => \timer[0]_i_5_n_0\,
      I2 => \timer[0]_i_6_n_0\,
      I3 => \timer[0]_i_7_n_0\,
      O => sel
    );
\timer[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => timer_reg(19),
      I1 => timer_reg(16),
      I2 => timer_reg(21),
      I3 => timer_reg(18),
      I4 => \timer[0]_i_9_n_0\,
      O => \timer[0]_i_4_n_0\
    );
\timer[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => timer_reg(4),
      I1 => timer_reg(13),
      I2 => timer_reg(3),
      I3 => timer_reg(2),
      I4 => \timer[0]_i_10_n_0\,
      O => \timer[0]_i_5_n_0\
    );
\timer[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => timer_reg(31),
      I1 => timer_reg(28),
      I2 => timer_reg(9),
      I3 => timer_reg(8),
      I4 => \timer[0]_i_11_n_0\,
      O => \timer[0]_i_6_n_0\
    );
\timer[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => timer_reg(27),
      I1 => timer_reg(26),
      I2 => timer_reg(6),
      I3 => timer_reg(5),
      I4 => \timer[0]_i_12_n_0\,
      O => \timer[0]_i_7_n_0\
    );
\timer[0]_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => timer_reg(0),
      O => \timer[0]_i_8_n_0\
    );
\timer[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => timer_reg(14),
      I1 => timer_reg(1),
      I2 => timer_reg(20),
      I3 => timer_reg(23),
      O => \timer[0]_i_9_n_0\
    );
\timer_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[0]_i_3_n_7\,
      Q => timer_reg(0),
      R => timer0
    );
\timer_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \timer_reg[0]_i_3_n_0\,
      CO(2) => \timer_reg[0]_i_3_n_1\,
      CO(1) => \timer_reg[0]_i_3_n_2\,
      CO(0) => \timer_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \timer_reg[0]_i_3_n_4\,
      O(2) => \timer_reg[0]_i_3_n_5\,
      O(1) => \timer_reg[0]_i_3_n_6\,
      O(0) => \timer_reg[0]_i_3_n_7\,
      S(3 downto 1) => timer_reg(3 downto 1),
      S(0) => \timer[0]_i_8_n_0\
    );
\timer_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[8]_i_1_n_5\,
      Q => timer_reg(10),
      R => timer0
    );
\timer_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[8]_i_1_n_4\,
      Q => timer_reg(11),
      R => timer0
    );
\timer_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[12]_i_1_n_7\,
      Q => timer_reg(12),
      R => timer0
    );
\timer_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[8]_i_1_n_0\,
      CO(3) => \timer_reg[12]_i_1_n_0\,
      CO(2) => \timer_reg[12]_i_1_n_1\,
      CO(1) => \timer_reg[12]_i_1_n_2\,
      CO(0) => \timer_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[12]_i_1_n_4\,
      O(2) => \timer_reg[12]_i_1_n_5\,
      O(1) => \timer_reg[12]_i_1_n_6\,
      O(0) => \timer_reg[12]_i_1_n_7\,
      S(3 downto 0) => timer_reg(15 downto 12)
    );
\timer_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[12]_i_1_n_6\,
      Q => timer_reg(13),
      R => timer0
    );
\timer_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[12]_i_1_n_5\,
      Q => timer_reg(14),
      R => timer0
    );
\timer_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[12]_i_1_n_4\,
      Q => timer_reg(15),
      R => timer0
    );
\timer_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[16]_i_1_n_7\,
      Q => timer_reg(16),
      R => timer0
    );
\timer_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[12]_i_1_n_0\,
      CO(3) => \timer_reg[16]_i_1_n_0\,
      CO(2) => \timer_reg[16]_i_1_n_1\,
      CO(1) => \timer_reg[16]_i_1_n_2\,
      CO(0) => \timer_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[16]_i_1_n_4\,
      O(2) => \timer_reg[16]_i_1_n_5\,
      O(1) => \timer_reg[16]_i_1_n_6\,
      O(0) => \timer_reg[16]_i_1_n_7\,
      S(3 downto 0) => timer_reg(19 downto 16)
    );
\timer_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[16]_i_1_n_6\,
      Q => timer_reg(17),
      R => timer0
    );
\timer_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[16]_i_1_n_5\,
      Q => timer_reg(18),
      R => timer0
    );
\timer_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[16]_i_1_n_4\,
      Q => timer_reg(19),
      R => timer0
    );
\timer_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[0]_i_3_n_6\,
      Q => timer_reg(1),
      R => timer0
    );
\timer_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[20]_i_1_n_7\,
      Q => timer_reg(20),
      R => timer0
    );
\timer_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[16]_i_1_n_0\,
      CO(3) => \timer_reg[20]_i_1_n_0\,
      CO(2) => \timer_reg[20]_i_1_n_1\,
      CO(1) => \timer_reg[20]_i_1_n_2\,
      CO(0) => \timer_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[20]_i_1_n_4\,
      O(2) => \timer_reg[20]_i_1_n_5\,
      O(1) => \timer_reg[20]_i_1_n_6\,
      O(0) => \timer_reg[20]_i_1_n_7\,
      S(3 downto 0) => timer_reg(23 downto 20)
    );
\timer_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[20]_i_1_n_6\,
      Q => timer_reg(21),
      R => timer0
    );
\timer_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[20]_i_1_n_5\,
      Q => timer_reg(22),
      R => timer0
    );
\timer_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[20]_i_1_n_4\,
      Q => timer_reg(23),
      R => timer0
    );
\timer_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[24]_i_1_n_7\,
      Q => timer_reg(24),
      R => timer0
    );
\timer_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[20]_i_1_n_0\,
      CO(3) => \timer_reg[24]_i_1_n_0\,
      CO(2) => \timer_reg[24]_i_1_n_1\,
      CO(1) => \timer_reg[24]_i_1_n_2\,
      CO(0) => \timer_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[24]_i_1_n_4\,
      O(2) => \timer_reg[24]_i_1_n_5\,
      O(1) => \timer_reg[24]_i_1_n_6\,
      O(0) => \timer_reg[24]_i_1_n_7\,
      S(3 downto 0) => timer_reg(27 downto 24)
    );
\timer_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[24]_i_1_n_6\,
      Q => timer_reg(25),
      R => timer0
    );
\timer_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[24]_i_1_n_5\,
      Q => timer_reg(26),
      R => timer0
    );
\timer_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[24]_i_1_n_4\,
      Q => timer_reg(27),
      R => timer0
    );
\timer_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[28]_i_1_n_7\,
      Q => timer_reg(28),
      R => timer0
    );
\timer_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[24]_i_1_n_0\,
      CO(3) => \NLW_timer_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \timer_reg[28]_i_1_n_1\,
      CO(1) => \timer_reg[28]_i_1_n_2\,
      CO(0) => \timer_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[28]_i_1_n_4\,
      O(2) => \timer_reg[28]_i_1_n_5\,
      O(1) => \timer_reg[28]_i_1_n_6\,
      O(0) => \timer_reg[28]_i_1_n_7\,
      S(3 downto 0) => timer_reg(31 downto 28)
    );
\timer_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[28]_i_1_n_6\,
      Q => timer_reg(29),
      R => timer0
    );
\timer_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[0]_i_3_n_5\,
      Q => timer_reg(2),
      R => timer0
    );
\timer_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[28]_i_1_n_5\,
      Q => timer_reg(30),
      R => timer0
    );
\timer_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[28]_i_1_n_4\,
      Q => timer_reg(31),
      R => timer0
    );
\timer_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[0]_i_3_n_4\,
      Q => timer_reg(3),
      R => timer0
    );
\timer_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[4]_i_1_n_7\,
      Q => timer_reg(4),
      R => timer0
    );
\timer_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[0]_i_3_n_0\,
      CO(3) => \timer_reg[4]_i_1_n_0\,
      CO(2) => \timer_reg[4]_i_1_n_1\,
      CO(1) => \timer_reg[4]_i_1_n_2\,
      CO(0) => \timer_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[4]_i_1_n_4\,
      O(2) => \timer_reg[4]_i_1_n_5\,
      O(1) => \timer_reg[4]_i_1_n_6\,
      O(0) => \timer_reg[4]_i_1_n_7\,
      S(3 downto 0) => timer_reg(7 downto 4)
    );
\timer_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[4]_i_1_n_6\,
      Q => timer_reg(5),
      R => timer0
    );
\timer_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[4]_i_1_n_5\,
      Q => timer_reg(6),
      R => timer0
    );
\timer_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[4]_i_1_n_4\,
      Q => timer_reg(7),
      R => timer0
    );
\timer_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[8]_i_1_n_7\,
      Q => timer_reg(8),
      R => timer0
    );
\timer_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \timer_reg[4]_i_1_n_0\,
      CO(3) => \timer_reg[8]_i_1_n_0\,
      CO(2) => \timer_reg[8]_i_1_n_1\,
      CO(1) => \timer_reg[8]_i_1_n_2\,
      CO(0) => \timer_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \timer_reg[8]_i_1_n_4\,
      O(2) => \timer_reg[8]_i_1_n_5\,
      O(1) => \timer_reg[8]_i_1_n_6\,
      O(0) => \timer_reg[8]_i_1_n_7\,
      S(3 downto 0) => timer_reg(11 downto 8)
    );
\timer_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => sel,
      D => \timer_reg[8]_i_1_n_6\,
      Q => timer_reg(9),
      R => timer0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0 is
  port (
    S_AXI_ARREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0 : entity is "modbusCRC_axi_v1_0";
end pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0;

architecture STRUCTURE of pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0 is
begin
modbusCRC_axi_v1_0_S00_AXI_inst: entity work.pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pd_modbusCRC_axi_0_0 is
  port (
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of pd_modbusCRC_axi_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of pd_modbusCRC_axi_0_0 : entity is "pd_modbusCRC_axi_0_0,modbusCRC_axi_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of pd_modbusCRC_axi_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of pd_modbusCRC_axi_0_0 : entity is "modbusCRC_axi_v1_0,Vivado 2017.3";
end pd_modbusCRC_axi_0_0;

architecture STRUCTURE of pd_modbusCRC_axi_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 64, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.pd_modbusCRC_axi_0_0_modbusCRC_axi_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
