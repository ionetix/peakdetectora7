
`timescale 1 ns / 1 ps

	module peakDetectMain_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 8
	)
	(
		// Users to add ports here
        input ADC_DOUTA_i,
        input ADC_DOUTB_i,
        output reg ADC_SCLK_o = 1,
        output reg ADC_CS_o = 1,
        output ADC_A0_o,
        output ADC_A1_o,
        output ADC_A2_o,

		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 3;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 64
	reg signed [31:0] f1frz = 0, f2frz = 0, f3frz = 0, f4frz = 0, f5frz = 0, f6frz = 0,
        r1frz = 0, r2frz = 0, r3frz = 0, r4frz = 0, r5frz = 0, r6frz = 0;
	reg [31:0] f1vpfrz = 0, f2vpfrz = 0, f3vpfrz = 0, f4vpfrz = 0, f5vpfrz = 0, f6vpfrz = 0,
        r1vpfrz = 0, r2vpfrz = 0, r3vpfrz = 0, r4vpfrz = 0, r5vpfrz = 0, r6vpfrz = 0;
	reg [31:0] f1rawfrz = 0, f2rawfrz = 0, f3rawfrz = 0, f4rawfrz = 0, f5rawfrz = 0, f6rawfrz = 0,
            r1rawfrz = 0, r2rawfrz = 0, r3rawfrz = 0, r4rawfrz = 0, r5rawfrz = 0, r6rawfrz = 0;
    reg freeze = 0;	
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

	always @( posedge S_AXI_ACLK )
	begin
        if (slv_reg_wren)
            freeze <= S_AXI_WDATA[0];
	end    

	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
	always @(*)
	begin
	      // Address decoding for reading registers
        case ( axi_araddr[7:2] )
        6'h00: reg_data_out = f1frz;
        6'h01: reg_data_out = r1frz;
        6'h02: reg_data_out = f2frz;
        6'h03: reg_data_out = r2frz;
        6'h04: reg_data_out = f3frz;
        6'h05: reg_data_out = r3frz;
        6'h06: reg_data_out = f4frz;
        6'h07: reg_data_out = r4frz;
        6'h08: reg_data_out = f5frz;
        6'h09: reg_data_out = r5frz;
        6'h0A: reg_data_out = f6frz;
        6'h0B: reg_data_out = r6frz;
        6'h0C: reg_data_out = f1vpfrz;
        6'h0D: reg_data_out = r1vpfrz;
        6'h0E: reg_data_out = f2vpfrz;
        6'h0F: reg_data_out = r2vpfrz;
        6'h10: reg_data_out = f3vpfrz;
        6'h11: reg_data_out = r3vpfrz;
        6'h12: reg_data_out = f4vpfrz;
        6'h13: reg_data_out = r4vpfrz;
        6'h14: reg_data_out = f5vpfrz;
        6'h15: reg_data_out = r5vpfrz;
        6'h16: reg_data_out = f6vpfrz;
        6'h17: reg_data_out = r6vpfrz;
        6'h18: reg_data_out = f1rawfrz;
        6'h19: reg_data_out = r1rawfrz;
        6'h1A: reg_data_out = f2rawfrz;
        6'h1B: reg_data_out = r2rawfrz;
        6'h1C: reg_data_out = f3rawfrz;
        6'h1D: reg_data_out = r3rawfrz;
        6'h1E: reg_data_out = f4rawfrz;
        6'h1F: reg_data_out = r4rawfrz;
        6'h20: reg_data_out = f5rawfrz;
        6'h21: reg_data_out = r5rawfrz;
        6'h22: reg_data_out = f6rawfrz;
        6'h23: reg_data_out = r6rawfrz;
        default: reg_data_out = 32'hFFFFFFFF;
        endcase
    end

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= reg_data_out;     // register read data
	        end   
	    end
	end    

	// Add user logic here
    reg [1:0] clkDiv = 0;                   // Divide bus clock by 4 to 25 MHz (will be further divided by 2 for 12.5 MHz sclk)
    reg [4:0] state = 0;                    // State counter for ADC read logic
    reg [11:0] aShift = 0, bShift = 0;      // Input shift registers for ADC data
    reg [11:0] aRaw = 0, bRaw = 0;          // Raw values from ADC
    wire signed [15:0] aDBM, bDBM;          // dBm values of ADC data before assigning to ADC channel
    wire [15:0] aVpADC, bVpADC;             // Vp values of ADC data before scaling and assigning to ADC channel
    wire [29:0] aVp, bVp;                   // Vp values of ADC data before assigning to ADC channel
    // Most recent dBm value for each channel (8.8 fixed point)
    reg signed [15:0] f1 = 0, f2 = 0, f3 = 0, f4 = 0, f5 = 0, f6 = 0, r1 = 0, r2 = 0, r3 = 0, r4 = 0, r5 = 0, r6 = 0;
    reg [29:0] f1vp = 0, f2vp = 0, f3vp = 0, f4vp = 0, f5vp = 0, f6vp = 0, r1vp = 0, r2vp = 0, r3vp = 0, r4vp = 0, r5vp = 0, r6vp = 0;
    reg [15:0] f1raw = 0, f2raw = 0, f3raw = 0, f4raw = 0, f5raw = 0, f6raw = 0, r1raw = 0, r2raw = 0, r3raw = 0, r4raw = 0, r5raw = 0, r6raw = 0;
    // Accumulator for each channel
    reg signed [31:0] f1avg = 0, f2avg = 0, f3avg = 0, f4avg = 0, f5avg = 0, f6avg = 0,
        r1avg = 0, r2avg = 0, r3avg = 0, r4avg = 0, r5avg = 0, r6avg = 0;
    reg [39:0] f1vpavg = 0, f2vpavg = 0, f3vpavg = 0, f4vpavg = 0, f5vpavg = 0, f6vpavg = 0,
            r1vpavg = 0, r2vpavg = 0, r3vpavg = 0, r4vpavg = 0, r5vpavg = 0, r6vpavg = 0;
    reg [31:0] f1rawavg = 0, f2rawavg = 0, f3rawavg = 0, f4rawavg = 0, f5rawavg = 0, f6rawavg = 0,
            r1rawavg = 0, r2rawavg = 0, r3rawavg = 0, r4rawavg = 0, r5rawavg = 0, r6rawavg = 0;
    // Output value for each channel (14.18 fixed point)
    reg signed [31:0] f1out = 0, f2out = 0, f3out = 0, f4out = 0, f5out = 0, f6out = 0,
        r1out = 0, r2out = 0, r3out = 0, r4out = 0, r5out = 0, r6out = 0;
    reg [31:0] f1vpout = 0, f2vpout = 0, f3vpout = 0, f4vpout = 0, f5vpout = 0, f6vpout = 0,
            r1vpout = 0, r2vpout = 0, r3vpout = 0, r4vpout = 0, r5vpout = 0, r6vpout = 0;
    reg [31:0] f1rawout = 0, f2rawout = 0, f3rawout = 0, f4rawout = 0, f5rawout = 0, f6rawout = 0,
            r1rawout = 0, r2rawout = 0, r3rawout = 0, r4rawout = 0, r5rawout = 0, r6rawout = 0;
    reg [2:0] channel = 0;                  // Channel counter for ADC addressing
    reg [2:0] channel_prev = 5;             // Channel for the current look-up table output
    reg [9:0] avgCtr = 0;                   // Counter for averaging 1024 samples
    reg validLUT = 0;                       // LUT output valid signal
    reg validChannels = 0;                  // All channels valid signal
    
    // Assignment of ADC address wires to channel counter value
    assign ADC_A0_o = channel[0];
    assign ADC_A1_o = channel[1];
    assign ADC_A2_o = channel[2];
    
    always @ (posedge S_AXI_ACLK)
    begin
        clkDiv <= clkDiv + 1'b1;                        // divide clock by 4 (to 25 MHz) for ADC control logic
        ADC_CS_o <= (state < 3);                        // set ADC chip select high between conversions
        ADC_SCLK_o <= state[0] | ~|state[4:2];          // toggle ADC clock at 12.5 MHz to read data
        validLUT <= ~|state & &clkDiv;                  // LUT valid 4 clock cycles after LUT input changes
        validChannels <= validLUT & ~|channel;          // register all channel values simultaneously one clock cycle after the last one is registered
        // ADC control logic operates at 1/4 bus clock frequency
        if (&clkDiv)
        begin
            state <= state + 1'b1;                      // state machine is just a counter
            // shift data in on falling edge of sclk (and also twice between reads) 
            if (~state[0])
            begin
                aShift <= {aShift[10:0], ADC_DOUTA_i};  // shift in data bit from ADC A channel
                bShift <= {bShift[10:0], ADC_DOUTB_i};  // shift in data bit from ADC B channel
            end
            // When all 12 bits are in the shift register, latch it
            if (state == 30)
            begin
                aRaw <= aShift;                         // register data word in ADC A channel shift register
                bRaw <= bShift;                         // register data word in ADC B channel shift register
            end
            // in final state, save the complete words in the shift registers and select the next channel
            if (&state)
            begin
                // increment channel counter and if it is the last channel increment the averaging counter
                if (channel == 3'h5)
                    channel <= 0;                       // reset channel counter to 0 after last channel
                else
                    channel <= channel + 1'b1;          // increment channel counter if not at last channel
                channel_prev <= channel;                // update channel in LUT table (always one channel behind ADC channel counter)
            end
        end
        // when output of LUT is valid, store in register for the corresponding channel
        if (validLUT)
        begin
            case (channel_prev)
            0:
            begin
                r6 <= aDBM + 16'h1100;                             // register reflected channel 6 data word
                f6 <= bDBM + 16'h1b00;                             // register forward channel 6 data word
                r6vp <= aVp;
                f6vp <= bVp;
                r6raw <= {4'h0, aRaw};
                f6raw <= {4'h0, bRaw};
            end
            1:
            begin
                r5 <= aDBM + 16'h1100;                             // register reflected channel 5 data word
                f5 <= bDBM + 16'h1b00;                             // register forward channel 5 data word
                r5vp <= aVp;
                f5vp <= bVp;
                r5raw <= {4'h0, aRaw};
                f5raw <= {4'h0, bRaw};
            end
            2:
            begin
                r4 <= aDBM + 16'h1100;                             // register reflected channel 4 data word
                f4 <= bDBM + 16'h1b00;                             // register forward channel 4 data word
                r4vp <= aVp;
                f4vp <= bVp;
                r4raw <= {4'h0, aRaw};
                f4raw <= {4'h0, bRaw};
            end
            3:
            begin
                r3 <= aDBM + 16'h1100;                             // register reflected channel 3 data word
                f3 <= bDBM + 16'h1b00;                             // register forward channel 3 data word
                r3vp <= aVp;
                f3vp <= bVp;
                r3raw <= {4'h0, aRaw};
                f3raw <= {4'h0, bRaw};
            end
            4:
            begin
                r2 <= aDBM + 16'h1100;                             // register reflected channel 2 data word
                f2 <= bDBM + 16'h1b00;                             // register forward channel 2 data word
                r2vp <= aVp;
                f2vp <= bVp;
                r2raw <= {4'h0, aRaw};
                f2raw <= {4'h0, bRaw};
            end
            5:
            begin
                r1 <= aDBM + 16'h1100;                             // register reflected channel 1 data word
                f1 <= bDBM + 16'h1b00;                             // register forward channel 1 data word
                r1vp <= aVp;
                f1vp <= bVp;
                r1raw <= {4'h0, aRaw};
                f1raw <= {4'h0, bRaw};
            end
            endcase
        end
        // after last channel is updated, simultaneously add all channels to the accumulated value
        if (validChannels)
        begin
            avgCtr <= avgCtr + 1'b1;                    // increment averaging counter
            // when averaging counter resets, register the final output and restart accumulating values 
            if (~|avgCtr)
            begin
                f1avg <= f1;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f2avg <= f2;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f3avg <= f3;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f4avg <= f4;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f5avg <= f5;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f6avg <= f6;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r1avg <= r1;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r2avg <= r2;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r3avg <= r3;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r4avg <= r4;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r5avg <= r5;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r6avg <= r6;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f1vpavg <= f1vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f2vpavg <= f2vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f3vpavg <= f3vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f4vpavg <= f4vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f5vpavg <= f5vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f6vpavg <= f6vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r1vpavg <= r1vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r2vpavg <= r2vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r3vpavg <= r3vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r4vpavg <= r4vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r5vpavg <= r5vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r6vpavg <= r6vp;    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f1rawavg <= {f1raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f2rawavg <= {f2raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f3rawavg <= {f3raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f4rawavg <= {f4raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f5rawavg <= {f5raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f6rawavg <= {f6raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r1rawavg <= {r1raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r2rawavg <= {r2raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r3rawavg <= {r3raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r4rawavg <= {r4raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r5rawavg <= {r5raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                r6rawavg <= {r6raw, 8'h00};    // VERIFY SIGN LOGIC    // load current value as initial value of accumulator
                f1out <= f1avg;                         // register accumulator output so it can be read on the AXI bus
                f2out <= f2avg;                         // register accumulator output so it can be read on the AXI bus
                f3out <= f3avg;                         // register accumulator output so it can be read on the AXI bus
                f4out <= f4avg;                         // register accumulator output so it can be read on the AXI bus
                f5out <= f5avg;                         // register accumulator output so it can be read on the AXI bus
                f6out <= f6avg;                         // register accumulator output so it can be read on the AXI bus
                r1out <= r1avg;                         // register accumulator output so it can be read on the AXI bus
                r2out <= r2avg;                         // register accumulator output so it can be read on the AXI bus
                r3out <= r3avg;                         // register accumulator output so it can be read on the AXI bus
                r4out <= r4avg;                         // register accumulator output so it can be read on the AXI bus
                r5out <= r5avg;                         // register accumulator output so it can be read on the AXI bus
                r6out <= r6avg;                         // register accumulator output so it can be read on the AXI bus
                f1vpout <= f1vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f2vpout <= f2vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f3vpout <= f3vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f4vpout <= f4vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f5vpout <= f5vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f6vpout <= f6vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r1vpout <= r1vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r2vpout <= r2vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r3vpout <= r3vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r4vpout <= r4vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r5vpout <= r5vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                r6vpout <= r6vpavg[39:16];                         // register accumulator output so it can be read on the AXI bus
                f1rawout <= f1rawavg;                         // register accumulator output so it can be read on the AXI bus
                f2rawout <= f2rawavg;                         // register accumulator output so it can be read on the AXI bus
                f3rawout <= f3rawavg;                         // register accumulator output so it can be read on the AXI bus
                f4rawout <= f4rawavg;                         // register accumulator output so it can be read on the AXI bus
                f5rawout <= f5rawavg;                         // register accumulator output so it can be read on the AXI bus
                f6rawout <= f6rawavg;                         // register accumulator output so it can be read on the AXI bus
                r1rawout <= r1rawavg;                         // register accumulator output so it can be read on the AXI bus
                r2rawout <= r2rawavg;                         // register accumulator output so it can be read on the AXI bus
                r3rawout <= r3rawavg;                         // register accumulator output so it can be read on the AXI bus
                r4rawout <= r4rawavg;                         // register accumulator output so it can be read on the AXI bus
                r5rawout <= r5rawavg;                         // register accumulator output so it can be read on the AXI bus
                r6rawout <= r6rawavg;                         // register accumulator output so it can be read on the AXI bus
            end
            else
            begin
                f1avg <= f1avg + f1;                    // add current value to accumulator
                f2avg <= f2avg + f2;                    // add current value to accumulator
                f3avg <= f3avg + f3;                    // add current value to accumulator
                f4avg <= f4avg + f4;                    // add current value to accumulator
                f5avg <= f5avg + f5;                    // add current value to accumulator
                f6avg <= f6avg + f6;                    // add current value to accumulator
                r1avg <= r1avg + r1;                    // add current value to accumulator
                r2avg <= r2avg + r2;                    // add current value to accumulator
                r3avg <= r3avg + r3;                    // add current value to accumulator
                r4avg <= r4avg + r4;                    // add current value to accumulator
                r5avg <= r5avg + r5;                    // add current value to accumulator
                r6avg <= r6avg + r6;                    // add current value to accumulator
                f1vpavg <= f1vpavg + f1vp;                    // add current value to accumulator
                f2vpavg <= f2vpavg + f2vp;                    // add current value to accumulator
                f3vpavg <= f3vpavg + f3vp;                    // add current value to accumulator
                f4vpavg <= f4vpavg + f4vp;                    // add current value to accumulator
                f5vpavg <= f5vpavg + f5vp;                    // add current value to accumulator
                f6vpavg <= f6vpavg + f6vp;                    // add current value to accumulator
                r1vpavg <= r1vpavg + r1vp;                    // add current value to accumulator
                r2vpavg <= r2vpavg + r2vp;                    // add current value to accumulator
                r3vpavg <= r3vpavg + r3vp;                    // add current value to accumulator
                r4vpavg <= r4vpavg + r4vp;                    // add current value to accumulator
                r5vpavg <= r5vpavg + r5vp;                    // add current value to accumulator
                r6vpavg <= r6vpavg + r6vp;                    // add current value to accumulator
                f1rawavg <= f1rawavg + {f1raw, 8'h00};                    // add current value to accumulator
                f2rawavg <= f2rawavg + {f2raw, 8'h00};                    // add current value to accumulator
                f3rawavg <= f3rawavg + {f3raw, 8'h00};                    // add current value to accumulator
                f4rawavg <= f4rawavg + {f4raw, 8'h00};                    // add current value to accumulator
                f5rawavg <= f5rawavg + {f5raw, 8'h00};                    // add current value to accumulator
                f6rawavg <= f6rawavg + {f6raw, 8'h00};                    // add current value to accumulator
                r1rawavg <= r1rawavg + {r1raw, 8'h00};                    // add current value to accumulator
                r2rawavg <= r2rawavg + {r2raw, 8'h00};                    // add current value to accumulator
                r3rawavg <= r3rawavg + {r3raw, 8'h00};                    // add current value to accumulator
                r4rawavg <= r4rawavg + {r4raw, 8'h00};                    // add current value to accumulator
                r5rawavg <= r5rawavg + {r5raw, 8'h00};                    // add current value to accumulator
                r6rawavg <= r6rawavg + {r6raw, 8'h00};                    // add current value to accumulator
            end
        end
        // if CPU has not frozen the register values, update them with the current values
        if (~freeze)
        begin
            f1frz <= f1out;                             // update value that can be read on the AXI bus
            f2frz <= f2out;                             // update value that can be read on the AXI bus
            f3frz <= f3out;                             // update value that can be read on the AXI bus
            f4frz <= f4out;                             // update value that can be read on the AXI bus
            f5frz <= f5out;                             // update value that can be read on the AXI bus
            f6frz <= f6out;                             // update value that can be read on the AXI bus
            r1frz <= r1out;                             // update value that can be read on the AXI bus
            r2frz <= r2out;                             // update value that can be read on the AXI bus
            r3frz <= r3out;                             // update value that can be read on the AXI bus
            r4frz <= r4out;                             // update value that can be read on the AXI bus
            r5frz <= r5out;                             // update value that can be read on the AXI bus
            r6frz <= r6out;                             // update value that can be read on the AXI bus
            f1vpfrz <= f1vpout;                             // update value that can be read on the AXI bus
            f2vpfrz <= f2vpout;                             // update value that can be read on the AXI bus
            f3vpfrz <= f3vpout;                             // update value that can be read on the AXI bus
            f4vpfrz <= f4vpout;                             // update value that can be read on the AXI bus
            f5vpfrz <= f5vpout;                             // update value that can be read on the AXI bus
            f6vpfrz <= f6vpout;                             // update value that can be read on the AXI bus
            r1vpfrz <= r1vpout;                             // update value that can be read on the AXI bus
            r2vpfrz <= r2vpout;                             // update value that can be read on the AXI bus
            r3vpfrz <= r3vpout;                             // update value that can be read on the AXI bus
            r4vpfrz <= r4vpout;                             // update value that can be read on the AXI bus
            r5vpfrz <= r5vpout;                             // update value that can be read on the AXI bus
            r6vpfrz <= r6vpout;                             // update value that can be read on the AXI bus
            f1rawfrz <= f1rawout;                             // update value that can be read on the AXI bus
            f2rawfrz <= f2rawout;                             // update value that can be read on the AXI bus
            f3rawfrz <= f3rawout;                             // update value that can be read on the AXI bus
            f4rawfrz <= f4rawout;                             // update value that can be read on the AXI bus
            f5rawfrz <= f5rawout;                             // update value that can be read on the AXI bus
            f6rawfrz <= f6rawout;                             // update value that can be read on the AXI bus
            r1rawfrz <= r1rawout;                             // update value that can be read on the AXI bus
            r2rawfrz <= r2rawout;                             // update value that can be read on the AXI bus
            r3rawfrz <= r3rawout;                             // update value that can be read on the AXI bus
            r4rawfrz <= r4rawout;                             // update value that can be read on the AXI bus
            r5rawfrz <= r5rawout;                             // update value that can be read on the AXI bus
            r6rawfrz <= r6rawout;                             // update value that can be read on the AXI bus
        end
    end
    
    // static look-up table that converts raw ADC samples to dBm in signed 8.8 fixed-point format
    blk_mem_gen_0 dBmLUT (.clka(S_AXI_ACLK), .addra(aRaw), .douta(aDBM),
        .clkb(S_AXI_ACLK), .addrb(bRaw), .doutb(bDBM));
    Vp_LUT vpLUT (.clka(S_AXI_ACLK), .addra(aRaw), .douta(aVpADC),
        .clkb(S_AXI_ACLK), .addrb(bRaw), .doutb(bVpADC));
    multA scaleA (.CLK(S_AXI_ACLK), .A(aVpADC), .P(aVp));
    multB scaleB (.CLK(S_AXI_ACLK), .A(bVpADC), .P(bVp));
     
	// User logic ends

	endmodule
