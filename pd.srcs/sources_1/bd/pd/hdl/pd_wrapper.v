//Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
//Date        : Fri Jan 31 15:06:24 2020
//Host        : Windows10 running 64-bit major release  (build 9200)
//Command     : generate_target pd_wrapper.bd
//Design      : pd_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module pd_wrapper
   (ADC_A0_o,
    ADC_A1_o,
    ADC_A2_o,
    ADC_CS_o,
    ADC_DOUTA_i,
    ADC_DOUTB_i,
    ADC_SCLK_o,
    clock_rtl,
    reset_rtl,
    uart_rtl_rxd,
    uart_rtl_txd);
  output ADC_A0_o;
  output ADC_A1_o;
  output ADC_A2_o;
  output ADC_CS_o;
  input ADC_DOUTA_i;
  input ADC_DOUTB_i;
  output ADC_SCLK_o;
  input clock_rtl;
  input reset_rtl;
  input uart_rtl_rxd;
  output uart_rtl_txd;

  wire ADC_A0_o;
  wire ADC_A1_o;
  wire ADC_A2_o;
  wire ADC_CS_o;
  wire ADC_DOUTA_i;
  wire ADC_DOUTB_i;
  wire ADC_SCLK_o;
  wire clock_rtl;
  wire reset_rtl;
  wire uart_rtl_rxd;
  wire uart_rtl_txd;

  pd pd_i
       (.ADC_A0_o(ADC_A0_o),
        .ADC_A1_o(ADC_A1_o),
        .ADC_A2_o(ADC_A2_o),
        .ADC_CS_o(ADC_CS_o),
        .ADC_DOUTA_i(ADC_DOUTA_i),
        .ADC_DOUTB_i(ADC_DOUTB_i),
        .ADC_SCLK_o(ADC_SCLK_o),
        .clock_rtl(clock_rtl),
        .reset_rtl(reset_rtl),
        .uart_rtl_rxd(uart_rtl_rxd),
        .uart_rtl_txd(uart_rtl_txd));
endmodule
