// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (win64) Build 2018833 Wed Oct  4 19:58:22 MDT 2017
// Date        : Wed Jan 15 20:27:03 2020
// Host        : Windows10 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               z:/Documents/git/master/pd/ip/peakDetectMain_1.0/src/blk_mem_gen_0/blk_mem_gen_0_stub.v
// Design      : blk_mem_gen_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a15tftg256-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_0,Vivado 2017.3" *)
module blk_mem_gen_0(clka, addra, douta, clkb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,addra[11:0],douta[15:0],clkb,addrb[11:0],doutb[15:0]" */;
  input clka;
  input [11:0]addra;
  output [15:0]douta;
  input clkb;
  input [11:0]addrb;
  output [15:0]doutb;
endmodule
